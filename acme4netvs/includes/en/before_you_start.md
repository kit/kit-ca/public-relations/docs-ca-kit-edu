## Before you start

The following explanations assume that you placed the netdb_config.ini from NETVS at either
`$USER/.config/netdb_client.ini` or in the working directory where the ACME client is run from.
An [example](https://gitlab.kit.edu/scc-net/netvs/netdb-client/-/blob/main/example_config.ini) for the config file can
be found in the [netdb-client repository](https://gitlab.kit.edu/scc-net/netvs/netdb-client)
For other options to pass the API token (via environment variable or command line argument),
please consult the help of the acme4netvs hooks with `-h`.

!!! warning
    If you are still testing certificate requests via ACME, please always use the
    [staging endpoint](https://letsencrypt.org/docs/staging-environment/) of Lets Encrypt.
    This will generate certificates that are not trusted by browsers, but will not trigger any rate limits of the production endpoint.
    If you trigger rate limiting, this might affect other users at KIT negatively. Be nice 🙂
