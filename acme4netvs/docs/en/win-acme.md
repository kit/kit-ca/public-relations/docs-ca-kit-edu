# win-acme

This is a short and opinionated guide, please consult the [upstream documentation](https://www.win-acme.com/manual/getting-started) for further details.

--8<-- "includes/en/before_you_start.md"

## Setup

Download the pluggable-version of win-acme as per instructions from the [upstream documentation](https://www.win-acme.com/manual/getting-started) and
extract the archive. This guide assumes a destination directory of `C:\win-acme`, adjust your process accordingly if you're using another directory.

Download the latest version of `acme4netvs_win-acme_x.y.z_windows_amd64.zip` [from the acme4netvs releases](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases).
Extract `acme4netvs_win-acme.exe` into `C:\win-acme\Scripts\`.

Create a directory `%UserProfile%\.config`. Inside, create a file `netdb_client.ini` and paste your NETVS token ini text into it.

!!! note
    Because Windows File Explorer does not allow to create folders with a dot as the first character, you will need to create the `.config` folder via commandline (`mkdir %UserProfile%\.config`) or powershell (`mkdir $home\.config`).

Run `c:\win-acme\wacs.exe --help` once. This will create the default `settings.json`. Edit this file to increase the
[script execution timeout value](https://www.win-acme.com/reference/settings#timeout) from 600 seconds to *25 minutes ✕ max number of SANs*. When in doubt, use *86400*:

```json
[…]
"Script": {
    "Timeout": 86400,
[…]
```

## Testing acme4netvs

To test if acme4netvs can read your token open a powershell terminal window. Execute this command after substituting these parts:

* `example.kit.edu` → one of the domains you actually plan to get certificates for on this machine
* `ab1234` → something unique that's easy to spot in [NETVS](https://netvs.scc.kit.edu/dnsvs/bcds)
* `0001` → increment on every subsequent invocation to correlate test runs with the results (or lack thereof)

```shell
C:\win-acme\Scripts\acme4netvs_win-acme.exe create example.kit.edu a User-ab1234-testing-win-acme-0001
```

You should be able to diagnose potential problems by reading the log output. The DNS record propagation (`Challenge is NOT yet available on …`) phase may take a while. If you're in hurry press Ctrl+C to abort.

Revert your test by running the same command with `delete` instead of `create`:

```shell
C:\win-acme\Scripts\acme4netvs_win-acme.exe delete example.kit.edu a User-ab1234-testing-win-acme-0001
```

## Testing acme4netvs together with win-acme

Open a powershell with elevated rights (“Run as administrator”) and enter this command after substituting the relevant parts.
The single backtick (**`**) is powershell's line continuation character for multiline inputs (it allows us to split this long commandline into multiple lines).

```shell
C:\win-acme\wacs.exe `
--test `
--accepttos --emailaddress beate.beispiel@kit.edu `
--notaskscheduler `
--source manual --host test01.oe.kit.edu,test02.oe.kit.edu `
--store pfxfile --pfxfilepath "C:\win-acme\" `
--validationmode dns-01 --validation script --dnsscript "C:\win-acme\Scripts\acme4netvs_win-acme.exe"
```

Here's a breakdown:

* `--test`: Use Let's Encrypts [staging endpoint](https://letsencrypt.org/docs/staging-environment/). Keep this while testing and remove in production. Add `--closeonfinish` to disable breaks during testing.
* `--accepttos`: Automatically accept Let's Encrypt's Terms of Service.
* ` --emailaddress beate.beispiel@kit.edu`: Change `beate.beispiel@kit.edu` to your or your team's email address.
* `--notaskscheduler`: Do not create (or offer to update) the scheduled task.
* `--source manual`: Get the list of names from the commandline. 
* `--host test01.oe.kit.edu,test02.oe.kit.edu`: Comma-separated list of all names to include in the certificate.
* `--store pfxfile`: Store key & certificate file PKCS12 (`.pfx`) file instead of the machine's certificate store.
* `--pfxfilepath "C:\win-acme\"`: Write key & certificate into `C:\win-acme\`.  
* `--validationmode dns-01`: Use DNS-01 validation (instead of default HTTP-01).
* `--validation script`: Use a script for DNS-01 validation.
* `--dnsscript "C:\win-acme\Scripts\acme4netvs_win-acme.exe"`: Use acme4netvs plugin at `C:\win-acme\Scripts\acme4netvs_win-acme.exe` for DNS-01 validation.

Add `--csr ec` if you want to try modern [elliptic curve cryptography](https://en.wikipedia.org/wiki/Elliptic-curve_cryptography).

<!--Add `--dnsscriptparallelism 3` to speed up DNS-01 validation (especially when you're requesting many SANs in a single certificate).
This only works if “`DisableParallelism` is set to `false` in `settings.config`” ([source](https://www.win-acme.com/reference/plugins/validation/dns/script#parallelism)).-->

win-acme may prefix every line of output from acme4netvs with `Script error: `. This can be ignored.

## Deployment

win-acme has many modes of operation for input, output, renewal, valiation and service notification which creates way too many permutations to list here.
Please read the [manual](https://www.win-acme.com/manual/getting-started) and the [command reference](https://www.win-acme.com/reference/cli) (also available via `wacs.exe --help`) for details.
`wacs.exe` also has an interactive menu when called without parameters.

IIS users should check out [the IIS source plugin](https://www.win-acme.com/reference/plugins/source/iis) (option `--source iis --host …`)
and [the IIS installation plugins](https://www.win-acme.com/reference/plugins/installation/iis) (option `--installation iis`) which fully automates certificate management for IIS vhosts.
See the *Unattended* sections for commandline examples.

### Caveats

* The task scheduler task automatically created by win-acme is executed by the `NT-AUTHORITY/SYSTEM` user. This breaks acme4netvs as `%UserProfile%` for SYSTEM ist a totally different path (`%WinDir%\system32\config\systemprofile`). You can either change the task to run as the correct user or create a symlink to the correct `.config` folder.
