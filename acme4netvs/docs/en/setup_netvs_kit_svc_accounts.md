# Setup using KIT service accounts

!!! danger "Don't use this anymore"
    This method has several disadvantages:
    
    * KIT service accounts have too many privileges. They are part of the general OU group(s)
      and have access to the OUs file server shares and potentially more (unexpected)
      services. They present a potential security risk with no added benefits over NETVS service accounts.
    * Getting KIT service accounts is disruptive as you have to wait for them to be created.
    * They are mildly obscure because they don't show up inside NETVS, IT officers have
      to manually manage their live cycles.

    We urge everyone [switching to NETVS service accounts](setup_netvs.md) to delete their now obsolete KIT service accounts.

### Pre-Setup
* [Create or request a service account](https://www.scc.kit.edu/dienste/8637.php)
* Wait for at least an 1 hour for the identity management synchronisation

### Assigning rights to the new service account
* Log in to NETVS (using your normal account).
* Go to https://netvs.scc.kit.edu/org/ou
* Select your OE, remember the »OE-Kurzbezeichnung«
* Make sure your account is listed as »OE-Betreuer«
  ![](assets/img/OE_Betreuer.png)
* Go to https://netvs.scc.kit.edu/cntl/groups (click on »Gruppen« in the left navigation bar)
* Create a new group »Neue Gruppe anlegen«
    * Important: Add the »OE-Kurzbezeichnung«
      ![](assets/img/Add_OE_Group.png)
* Add your Service Account as Group Member (Gruppenmitglieder → +)
  ![](assets/img/Add_Group_Member_To_Group.png)
* Add Domains to your Group (Domains → +)
  ![](assets/img/Add_Domain.png)

### Creating Tokens

!!! note
    All steps happen on the Serviceaccount, not on your normal account

* Log in to NETVS using the Serviceaccount (you can use the »incognito mode« of your browser)
* In the upper right, click on your service account name and then »Unterkonten & API-Tokens«
* Click on »Unterkonto erstellen«
  ![](assets/img/Add_Subaccount.png)
    * Add a nice description
    * Deactivate "alle Rollen übernehmen"
    * Activate (don't change preset) "Kontozugriff auf datenmodifizierende Funktionen"
* Open the newly created subaccount, go to »Tokens«, click on »+«
* Add a description, click on »Anlegen«, run the transaction
* You now have a token (but without permissions).

### Add at least one subgroup

!!! note
    All steps happen on the Serviceaccount, not on your normal account

* Now, create a subgroup (Untergruppen → »Neue Untergruppe anlegen und beitreten«)
* Depending on your permission management, there are two ways:

    1. Create a group for every domain or server.
        * Un-Tick »Zuordnungen der Hauptgruppe übernehmen«
        * Add domains to the subgroup (as in the previous step)
    2. Create one group to rule them all.
       Leave the tick at »Zuordnungen der Hauptgruppe übernehmen«


### Assign permission to the subaccount

!!! note
    All steps happen on the Serviceaccount, not on your normal account

* You must add the subaccount to the right subgroup.
* Copy the subaccount ID (Pattern: `XXXXXXXXXXX==[SUBM]`)
* Go to the Groups, select the previously created subgroup.
* Click on Mitglieder and  »+«
* Paste the subaccount ID into »Konto-Benutzerkennung«
