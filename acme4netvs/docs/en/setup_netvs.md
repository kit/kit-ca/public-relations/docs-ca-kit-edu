# Setup NETVS

!!! warning
    If your organizational unit is managed by ATIS (Abteilung Technische Infrastruktur), this process will most
    likely not work for you, because ATIS adds another management layer on top of NETVS.
    Please contact ATIS for how to work with ACME.

!!! info "Deprecation notice for KIT service account method"
    This document previously recommended using KIT-global service accounts. This practice has been superseded by
    NETVS service accounts. We urge all users to follow this guide for new installations. Migrating away from
    KIT-global service account method is strongly encouraged in the medium term.
    
    [The old method is still documented here](setup_netvs_kit_svc_accounts.md){ :target="_blank" }. 

## Setup using NETVS Service Accounts

This process supports _split responsibilities_ where the people that need certificates (called _certificate users_ in
this guide) to provide services may not also be IT officers („IT-Beauftrage“) or NETVS OU administrators.

### Create a NETVS Service Account

!!! note "Check your permissions"
    You need to have write access to all domains for which you want to create certificates. Please check that you are
    an OU administrator („OE-Betreuer“) in the respective NETVS OU. See the [NETVS OU Page](https://netvs.scc.kit.edu/org/ou) to check your OU assignments.

* Navigate to [NETVS Hub](https://netvs.scc.kit.edu/tools/hub)
* Click "Execute" on "Service Account for ACME"

![](img/netvs_hub_startpage.webp){ width="924" }

* Fill in the form. Please check the description of the fields for details about their contents. Add all certificate
  users under Managing Accounts. 

![](img/netvs_hub_service-account-for-acme_filled_form.webp){ width="500" }

* Execute the transaction.

![](img/netvs_hub_service-account-for-acme_apply.webp){ width="378" }

The new NETVS service account can be found on the [NETVS Service Accounts page](https://netvs.scc.kit.edu/cntl/svc-accounts).

### Create Subgroup and Token

!!! note
    For the following steps, it is necessary that you share a group with the previously created NETVS Service Account.
    OU administrators can delegate service account permissions to other accounts (_certificate users_) by adding
    them to the _Managing Accounts_ field in the _Service Account for ACME_ dialogue.
    
    If you can see the Service Account in the [NETVS Service Accounts page](https://netvs.scc.kit.edu/cntl/svc-accounts),
    you possess all required permissions.

* Navigate to the [NETVS Service Accounts Page](https://netvs.scc.kit.edu/cntl/svc-accounts).
    * Take note of the group name which has access to the desired domain, you will need it later.

![](img/netvs_svc_account_note_group_name.webp){ width="865" }

* Click the "Impersonate" button for the respective service account.

![](img/netvs_svc_account_impersonate.webp){ width="865" }

* The NETVS is now seen from the "perspective" of the service account (indicated in the page header).

![](img/netvs_svc_account_impersonate_header.webp){ width="985" }

* Navigate to [NETVS Hub](https://netvs.scc.kit.edu/tools/hub).
* Click "Execute" on "Create API Token for FQDNs".

![](img/netvs_hub_startpage_create_token.webp){ width="924" }

* Fill in the form.
    * Use the group previously noted as "Group".
    * Fill in the domains for which you want to request certificates for.

![](img/netvs_hub_api-token_filled_form.webp){ width="500" }

* Execute the transaction.

![](img/netvs_hub_api-token_apply.webp){ width="288" }

* You receive a token which possesses all required permissions to use it with ACME4NETVS.

![](img/netvs_hub_api-token_result.webp){ width="800" }
