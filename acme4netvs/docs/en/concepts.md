This document aims to describe a generic way of obtaining X.509 server certificates from an [ACME](https://datatracker.ietf.org/doc/html/rfc8555)-enabled certification authority using the [DNS-01](https://letsencrypt.org/de/docs/challenge-types/#dns-01-challenge) challenge.
While there are a few certification authorities that offer ACME, this guide will only focus on [Let's Encrypt](https://letsencrypt.org/).

## ACME Challenges

The most common ACME Challenge Types are the HTTP-01 Challenge and the DNS-01 Challenge.

### HTTP-01 Challenge

This method is suitable if you run a publicy available webserver, and you don't want to obtain wildcard certificates.
Your acme client requests a challenge string and places it in a file at a well-known location in the webroot.
The CA requests this file and issues the certificate for this server if the challenge was correct.
You can find a longer description on the [Let's Encrypt website](https://letsencrypt.org/de/docs/challenge-types/#dns-01-challenge).

This of course means that this method does not work for servers that are not reachable from the internet.

!!! info
    You do not need `acme4netvs` to perform HTTP-01 challenges.

### DNS-01 Challenge

For this type of challenge, the challenge string is published to the internet through a [DNS TXT Resource Record](https://de.wikipedia.org/wiki/TXT_Resource_Record).
This method has two advantages over the HTTP-01 challenge: 

- You can request certificates including a wildcard domain (*.example.com) which is valid for all subdomains
- The system does not need to be reachable from the internet, only the DNS server(s) has/have to be reachable

In order to deploy the challenge string to DNS, we provide the acme4netvs plugin documented on this page.

!!! note
    Your system does not have to be reachable from the internet itself (no incoming connections necessary), but must be
    able to contact `www-net.scc.kit.edu` and **the CA server on the internet** (outgoing communication must be possible).
    If your system is not allowed to contact servers in the broader internet, you need a service system that requests the
    certificate for it and deploys the resulting certificate correctly.

!!! info
    You **do** need `acme4netvs` to perform DNS-01 challenges for domain names that are managed with
    [DNSVS](https://www.scc.kit.edu/en/services/3752.php){ :target="_blank" }.

Here is a simplified diagram of how DNS-01 challenges work with acme4netvs:

![](img/acme4netvs_flow_diagram.webp){ width="1322" }
