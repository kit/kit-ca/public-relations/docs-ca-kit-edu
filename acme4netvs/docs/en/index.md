# Obtaining X.509 certificates from Let's Encrypt at KIT

This document explains how to use the [acme4netvs](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs)
software to acquire server certificates from [Let's Encrypt](https://letsencrypt.org) at the Karlsruhe Institute of Technology.

This documentation is primarily targeted at server operators at the [Karlsruhe Institute of Technology](https://www.kit.edu).
Please talk to you institute's IT staff in case you require server certificates but are not an OU administrator yourself.

## There are so many options here, what do I do?

0. Check out the [Concepts](concepts.md) if you are interested in what is happening here
1. Follow the [Installation](installation.md) instructions
2. [Setup your NETVS organisation](setup_netvs.md) for ACME
3. Choose an [ACME client](https://letsencrypt.org/docs/client-options/) and read our documentation for it (or theirs, if we do not provide one). We currently document these ACME clients:
    * [Dehydrated](dehydrated.md)
    * [Certbot](certbot.md)
    * [acme.sh](acme.sh.md)
    * [win-acme](win-acme.md)
    * [Proxmox](proxmox.md)

Which ACME client you choose may depend on your specific requirements and environments as well as you personal preferences.

If you have no idea what to do, the acme4netvs maintainer's personal recommendations are: use [win-acme](win-acme.md) on Windows and [dehydrated](dehydrated.md) for everything else.

If you wonder how to integrate an ACME client / Let's Encrypt in your specific service, check out the [contributed instructions](contrib.md) from other members of KIT!

## Documentation enhancements

This source for this documentation is located at [https://gitlab.kit.edu/kit/kit-ca/public-relations/docs-ca-kit-edu](https://gitlab.kit.edu/kit/kit-ca/public-relations/docs-ca-kit-edu) and open for merge requests. Feel free to contribute enhancements, fixes and clarifications.

## Community chat 

We have a community chat on KIT Matrix (join link for [Desktop Client](https://matrix.to/#/#acme4netvs-community:kit.edu) and [KIT's web client](https://element.matrix.kit.edu/#/room/#scc:kit.edu)) for discussions, questions and announcements. We strongly suggest joining if you are using acme4netvs.  
