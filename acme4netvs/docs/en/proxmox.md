# Proxmox

!!! warning 
    You don't need a netvs-config file for the proxmox installation. The API-Token is provided via webinterface.

You can use this hook-script to automatically generate Certificates on your Proxmox-Hosts (tested for PVE. PBS and PMG might also work but have not been tested).
Please be aware if you have a proxmox-cluster you need to install packages / modify code each host of your cluster.

### Package

The package is available in the [KIT-CA Debian Repository](installation.md).

1. Install it with `apt-get install acme4netvs-proxmox`
2. Setup ACME via the Proxmox Webinterface and provide the API Token at the plugin setup.

### Manual
There is a modification and a symlink needed after installing the `acme4netvs` package on your proxmox-host.
Following modifications are required:

1. Install the CA-Repository for Debian as described in the [article](installation.md) and install the package `acme4netvs` on your proxmox host(s).
2. Create NETVS API-Token as [described](setup_netvs.md).
3. Proxmox file modifications:
    * create a symlink from `/usr/libexec/acme4netvs/dns_acme4netvs.sh` to `/usr/share/proxmox-acme/dnsapi/dns_acme4netvs.sh`: `ln -s /usr/libexec/acme4netvs/dns_acme4netvs.sh /usr/share/proxmox-acme/dnsapi/dns_acme4netvs.sh`
    * insert folowing into `/usr/share/proxmox-acme/dns-challenge-schema.json`:
```json
"acme4netvs": {
    "fields": {
        "NETDB_API_TOKEN": {
            "description": "The netdb API key",
            "type": "string"
        }
    }
},
```
!!! warning
    Pay attention to the file syntax so the result is still valid JSON.

4. Restart `pveproxy` and `pvedaemon`: `systemctl restart pveprox pvedaemon`
5. Setup ACME via the Proxmox Webinterface and provide the API Token at the plugin setup.
