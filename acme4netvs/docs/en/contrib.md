# Contributed Documentation

Several people have written documentation and tools for other specific use cases and integrations.

--8<-- "includes/en/before_you_start.md"

## Traefik & Docker

Patrick von der Hagen [has published files and a short documentation
(in German)](https://gitlab.kit.edu/patrick.hagen/traefik2-netvs) on how to use acme4netvs with
[Traefik](https://doc.traefik.io/traefik/) and [Docker](https://www.docker.com/). 

## RDP integration for win-acme

[https://gitlab.kit.edu/kit/ieh/it/certs/rdp-handler](https://gitlab.kit.edu/kit/ieh/it/certs/rdp-handler)

## acme4netvs TrueNAS Installer

[https://gitlab.kit.edu/kit/ieh/it/certs/acme4netvs-truenas-installer](https://gitlab.kit.edu/kit/ieh/it/certs/acme4netvs-truenas-installer)
