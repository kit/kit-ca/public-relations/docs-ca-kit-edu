# acme.sh

For acme.sh, we provide a wrapper script.
This is only a short manual, for a more detailed documentation see the [official acme.sh documentation](https://github.com/acmesh-official/acme.sh/wiki).

--8<-- "includes/en/before_you_start.md"

## Installation / Account-Registration

For installation of acme.sh and registration of your letsencrypt account please refer to the [official guide](https://github.com/acmesh-official/acme.sh#1-how-to-install).

## Setup

You have to install the wrapper script to a path where `acme.sh` searches for it. Place the [`dns_acme4netvs.sh`](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/blob/main/wrapper/dns_acme4netvs.sh) script inside the `~/.acme.sh/` or `~/.acme.sh/dnsapi/` folder of the user
which runs `acme.sh` or create a symlink to it from one of the aforementioned folders.

!!! note
    Since v3, [`acme.sh` uses Zerossl as the default Certificate Authority (CA)](https://github.com/acmesh-official/acme.sh/wiki/ZeroSSL.com-CA). Use `--server letsencrypt` to explicitly select Let's Encrypt.

!!! warning
    `acme.sh` is currently broken on plattforms like FreeBSD which ship a restricted `sh` shell instead of symlinking `sh` to `bash` (like most Linux distributions). For an easy fix install `bash` and change the very first line in `acme.sh` accordingly (substitute `sh` for `bash`).

## Issue certificate

```
acme.sh --server letsencrypt --issue --dns dns_acme4netvs -d example.com
```
