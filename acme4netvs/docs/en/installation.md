# Installation

##  Debian & Ubuntu

The simplest way is to add the KIT-CA Repository.
The repository is available for bullseye, buster (Debian), focal and jammy (Ubuntu)

###  Add the KIT-CA Repo

!!! tip "repository signing key path changed"
    Early versions of this documentation named the repository signing key `ca-repo.gpg` instead of
    `kit-ca-repo.asc`. Make sure to fix the `Signed-By:` line in your old configuration if neccessary.

#### Manual

* Install the basic packages:
```sh
apt-get install -y apt-transport-https curl gpg wget
```
* Download the KIT-CA GPG Key:
```sh
wget https://repo.ca.kit.edu/keys/repo_at_ca_kit_edu \
 -O /usr/share/keyrings/kit-ca-repo.asc
```
* Add the sources file `/etc/apt/sources.list.d/kit-ca-repo.sources` (change `bookworm` to your distribution):
```sh
cat > /etc/apt/sources.list.d/kit-ca-repo.sources <<'EOF'
Enabled: yes
X-Repolib-Name: ca-repo
Types: deb
Components: main
Suites: bookworm
URIs: https://repo.ca.kit.edu/
Signed-By: /usr/share/keyrings/kit-ca-repo.asc
EOF
```
* Update package database:
```sh
apt-get update
```
* **Important:** Install the package `kit-ca-repo-key`:
```sh
apt-get install -y kit-ca-repo-key
```

#### Via Package

* Go to [https://repo.ca.kit.edu/pool/main/k/kit-ca-repo/](https://repo.ca.kit.edu/pool/main/k/kit-ca-repo/) and get the correct link for the package `kit-ca-repo-apt-source`
* Download the package and install it:
```sh
. /etc/os-release; cd $(mktemp -d);  wget https://repo.ca.kit.edu/pool/main/k/kit-ca-repo/kit-ca-repo-apt-source_0.1.3-1%2B${VERSION_CODENAME}1_amd64.deb; dpkg -i *.deb'
```

### Install acme4netvs + ACME client

* `apt-get install -y acme4netvs dehydrated` or
* `apt-get install -y acme4netvs certbot`

## Everything else

You can download the most recent version of acme4netvs for your respective plattform via permalinks to our GitLab releases:

| dehydrated                                                                                                                                  	| certbot                                                                                                                                  	| win-acme                                                                                                                             	|
|---------------------------------------------------------------------------------------------------------------------------------------------	|------------------------------------------------------------------------------------------------------------------------------------------	|--------------------------------------------------------------------------------------------------------------------------------------	|
| 🐧 [Linux x64](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_dehydrated_linux_amd64.tar.gz)       	| 🐧 [Linux x64](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_certbot_linux_amd64.tar.gz)       	| ❌                                                                                                                                    	|
| 🐧 [Linux ARM64v7](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_dehydrated_linux_arm64v7.tar.gz) 	| 🐧 [Linux ARM64v7](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_certbot_linux_arm64v7.tar.gz) 	| ❌                                                                                                                                    	|
| 🐧 [Linux ARM64v6](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_dehydrated_linux_arm64.tar.gz)   	| 🐧 [Linux ARM64v6](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_certbot_linux_arm64.tar.gz)   	| ❌                                                                                                                                    	|
| 🐧 [Linux ARMv7](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_dehydrated_linux_armv7.tar.gz)     	| 🐧 [Linux ARMv7](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_certbot_linux_armv7.tar.gz)     	| ❌                                                                                                                                    	|
| 🐧 [Linux ARMv6](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_dehydrated_linux_armv6.tar.gz)     	| 🐧 [Linux ARMv6](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_certbot_linux_armv6.tar.gz)     	| ❌                                                                                                                                    	|
| 😈 [FreeBSD x64](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_dehydrated_freebsd_amd64.tar.gz)   	| 😈 [FreeBSD x64](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_certbot_freebsd_amd64.tar.gz)   	| ❌                                                                                                                                    	|
| 🍎 [MacOS All](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_dehydrated_darwin_all.tar.gz)        	| 🍎 [MacOS All](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_certbot_darwin_all.tar.gz)        	| ❌                                                                                                                                    	|
| 🍎 [MacOS x64](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_dehydrated_darwin_amd64.tar.gz)      	| 🍎 [MacOS x64](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_certbot_darwin_amd64.tar.gz)      	| ❌                                                                                                                                    	|
| 🍎 [MacOS ARM64](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_dehydrated_darwin_arm64.tar.gz)    	| 🍎 [MacOS ARM64](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_certbot_darwin_arm64.tar.gz)    	| ❌                                                                                                                                    	|
| 🪟 [Windows x64](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_dehydrated_windows_amd64.zip)      	| 🪟 [Windows x64](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_certbot_windows_amd64.zip)      	| 🪟 [Windows x64](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases/permalink/latest/downloads/acme4netvs_win-acme_windows_amd64.zip) 	|
