# Certbot

This is a short and opinionated guide, please consult the [official documentation for certbot](https://eff-certbot.readthedocs.io) for further details.

--8<-- "includes/en/before_you_start.md"

## Installation

First, you need to install certbot.
For most Linux distributions, certbot is available via the main package sources and can be installed via the respective package manager.

Examples:

* Debian/Ubuntu: `apt install certbot`
* Fedora: `dnf install certbot`
* Arch: `pacman install certbot`

Certbot is also available via the snap store
```
snap install certbot
```
and the python package manager `pip`
```
pip install certbot
```

## Initial certificate request

You first need to run certbot in order to register an ACME account and get the initial certificate for the domain.
As we want to use the DNS-01 challenge instead of HTTP-01, we need to request only a certificate without any webservers used.
This is possible with the `certonly --manual` mode of certbot:

**For testing**
```bash
certbot certonly --manual --manual-auth-hook /usr/libexec/acme4netvs/certbot_netvs_auth_hook --manual-cleanup-hook /usr/libexec/acme4netvs/certbot_netvs_cleanup_hook --preferred-challenge=dns --test-cert -d your-subdomain.example.kit.edu
```

**For production certificates**
```bash
certbot certonly --manual --manual-auth-hook /usr/libexec/acme4netvs/certbot_netvs_auth_hook --manual-cleanup-hook /usr/libexec/acme4netvs/certbot_netvs_cleanup_hook --preferred-challenge=dns -d your-subdomain.example.kit.edu
```

**Explanation**

- `--manual` enables a mode of certbot where we can supply our own scripts for deploying challenges
- `--manual-auth-hook` and `--manual-cleanup-hook` supply the scripts for deploying and cleaning up the challenge from the KIT DNS. In this example, we used the paths as they are in the debian package, please adapt according to your installation.
- `--preferred-challenge=dns` instructs certbot to use only DNS-01
- `--test-cert` enables the use of [staging endpoint](https://letsencrypt.org/docs/staging-environment/) of Lets Encrypt (only for testing)
- `-d your-subdomain.example.kit.edu` sets the domain that the certificate ist requested for. You can pass `-d` multiple times if you need multiple domains in one certificate (Subject Alternative Names)

If you never ran Certbot before, this will ask you for a contact mail address and some agreements.
Please answer them at your discretion, but mind that you must accept the terms and conditions to use Lets Encrypt.
Also, after testing with the staging endpoint, you need to re-enter this information once you switch to the production endpoint as they use different accounts.

```bash
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator manual, Installer None
Enter email address (used for urgent renewal and security notices)
 (Enter 'c' to cancel): your-team-address@example.kit.edu

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please read the Terms of Service at
https://letsencrypt.org/documents/LE-SA-v1.3-September-21-2022.pdf. You must
agree in order to register with the ACME server. Do you agree?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: Y

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Would you be willing, once your first certificate is successfully issued, to
share your email address with the Electronic Frontier Foundation, a founding
partner of the Let's Encrypt project and the non-profit organization that
develops Certbot? We'd like to send you email about our work encrypting the web,
EFF news, campaigns, and ways to support digital freedom.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: N
Account registered.
```

Then, certbot continues with the certificate request procedure.
First, it runs the `manual-auth-hook` for deploying the challenge as a DNS record.

!!! warning
    For `kit.edu` domains, creating the DNS challenge currently takes about 20 minutes for each domain in the certificate.
    `acme4netvs` waits for all DNS servers to actually have the challenge available.
    This is expected behaviour for now.

    In the future, KIT's DNS servers will allow for real-time updates, deploying a challenge will then only take a few seconds.

!!! note
    certbot caches output of plugins and therefore also the output of `acme4netvs`.
    certbot will seem stuck on "Running manual-auth-hook command" until the acme4netvs plugin is finished.

```bash
Requesting a certificate for your-subdomain.example.kit.edu
Performing the following challenges:
dns-01 challenge for your-subdomain.example.kit.edu
Running manual-auth-hook command: /usr/libexec/acme4netvs/certbot_netvs_auth_hook
Output from manual-auth-hook command certbot_netvs_auth_hook:
acme4netvs 2022/10/12 13:28:40 ✨ Using config file /root/.config/netdb_client.ini
acme4netvs 2022/10/12 13:28:40 🚀 NETVS client created (baseURI: »www-net.scc.kit.edu«, apiVersion: »3.2«)
acme4netvs 2022/10/12 13:28:40 ✨ [your-subdomain.example.kit.edu] Creating domain record
acme4netvs 2022/10/12 13:28:40 ✨ [your-subdomain.example.kit.edu] Creating _acme-challenge domain record
acme4netvs 2022/10/12 13:28:40 ✨ [your-subdomain.example.kit.edu] Creating _acme-challenge TXT record
acme4netvs 2022/10/12 13:28:40 ✨ [your-subdomain.example.kit.edu] has nameservers ns-t-1.kit-dns.eu., ns-t-2.kit-dns.de.
acme4netvs 2022/10/12 13:28:40 ✨ Waiting for 2 parallel nameserver checks
acme4netvs 2022/10/12 13:28:40 ✨ [your-subdomain.example.kit.edu] Started new goroutine to check DNS challenge on ns-t-2.kit-dns.de.
acme4netvs 2022/10/12 13:28:40 ✨ [your-subdomain.example.kit.edu] Started new goroutine to check DNS challenge on ns-t-1.kit-dns.eu.
acme4netvs 2022/10/12 13:28:41 ✅ [your-subdomain.example.kit.edu] Challenge is available on ns-t-2.kit-dns.de. (after 503.415651ms)
acme4netvs 2022/10/12 13:28:41 ✅ [your-subdomain.example.kit.edu] Challenge is available on ns-t-1.kit-dns.eu. (after 503.719462ms)
acme4netvs 2022/10/12 13:28:41 👍 [your-subdomain.example.kit.edu] Nameserver checks are done
```

Afterwards, certbot tells Lets Encrypt to verify the deployed challenge.

```bash
Waiting for verification...
```

Lastly, it cleans up the challenge and congratulates you to your shiny new certificate.

```bash
Cleaning up challenges
Running manual-cleanup-hook command: /usr/libexec/acme4netvs/certbot_netvs_auth_hook
Output from manual-cleanup-hook command certbot_netvs_auth_hook:
acme4netvs 2022/10/12 13:28:45 ✨ Using config file /root/.config/netdb_client.ini
acme4netvs 2022/10/12 13:28:45 🚀 NETVS client created (baseURI: »www-net.scc.kit.edu«, apiVersion: »3.2«)
acme4netvs 2022/10/12 13:28:46 ✨ [your-subdomain.example.kit.edu] has nameservers ns-t-2.kit-dns.de., ns-t-1.kit-dns.eu.
acme4netvs 2022/10/12 13:28:46 ✨ Waiting for 2 parallel nameserver checks
acme4netvs 2022/10/12 13:28:46 ✨ [your-subdomain.example.kit.edu] Started new goroutine to check DNS challenge on ns-t-1.kit-dns.eu.
acme4netvs 2022/10/12 13:28:46 ✨ [your-subdomain.example.kit.edu] Started new goroutine to check DNS challenge on ns-t-2.kit-dns.de.
acme4netvs 2022/10/12 13:28:46 ✅ [your-subdomain.example.kit.edu] Challenge is available on ns-t-1.kit-dns.eu. (after 503.636406ms)
acme4netvs 2022/10/12 13:28:46 ✅ [your-subdomain.example.kit.edu] Challenge is available on ns-t-2.kit-dns.de. (after 504.721566ms)
acme4netvs 2022/10/12 13:28:46 👍 [your-subdomain.example.kit.edu] Nameserver checks are done


IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/your-subdomain.example.kit.edu/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/your-subdomain.example.kit.edu/privkey.pem
   Your certificate will expire on 2023-01-10. To obtain a new or
   tweaked version of this certificate in the future, simply run
   certbot again. To non-interactively renew *all* of your
   certificates, run "certbot renew"
```

Your certificate files are now symlinked from `/etc/letsencrypt/live/$domain/` and can be used by a service from this path or deployed to different machines.
When the certificate is renewed, the symlinks are automatically updated.

## Renewal
To check if a certificate is up for renewal, just run `certbot renew` regularly.
This will automatically request a new certificate with remembered information from the initial run of `certbot certonly` if it is up for renewal.

**Certificate not up for renewal**
```bash
Saving debug log to /var/log/letsencrypt/letsencrypt.log

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Processing /etc/letsencrypt/renewal/your-subdomain.example.kit.edu.conf
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Cert not yet due for renewal

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
The following certificates are not due for renewal yet:
  /etc/letsencrypt/live/your-subdomain.example.kit.edu/fullchain.pem expires on 2023-01-10 (skipped)
No renewals were attempted.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```

**Certificate up for renewal**
```bash
Saving debug log to /var/log/letsencrypt/letsencrypt.log

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Processing /etc/letsencrypt/renewal/your-subdomain.example.kit.edu.conf
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Plugins selected: Authenticator manual, Installer None
Renewing an existing certificate for your-subdomain.example.kit.edu

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
new certificate deployed without reload, fullchain is
/etc/letsencrypt/live/your-subdomain.example.kit.edu/fullchain.pem
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Congratulations, all renewals succeeded: 
  /etc/letsencrypt/live/your-subdomain.example.kit.edu/fullchain.pem (success)
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```

Regular runs can be done for example with [cron](https://en.wikipedia.org/wiki/Cron) or [systemd timers](https://wiki.archlinux.org/title/systemd/Timers).
Please mind that `certbot renew` has to be run as the same user that ran the initial `certbot certonly` command in order to be allowed to access and modify files in `/etc/letsencrypt`.
