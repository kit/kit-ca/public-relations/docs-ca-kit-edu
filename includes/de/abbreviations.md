*[Sectigo]: Eine Firma aus UK, die digitale Zertifikate vertreibt
*[TCS]: Trusted Certificate Services, ein Dienst von GÉANT zum Bezug von Zertifikaten für Forschungseinrichtungen
*[GÉANT]: Zusammenschluss der europäischen Forschungs- und Ausbildungs-Netzwerke (European National Research and Education Networks – NRENs)
*[KIT-CA]: Die zentrale Zertifizierungsstelle des KIT
*[EULA]: Endbenutzer-Lizenzvertrag (aus dem Englischen “end-user license agreement”)
*[TripleDES]: Ein Verschlüsselungsverfahren, dass seit vielen Jahren als unsicher gilt
*[SHA1]: Ein Hashverfahren, das seit vielen Jahren als kryptographisch unsicher gilt
*[PKCS12]: Archivformat für kryptographische Objekte; hier normalerweise ein privater Schlüssel, ein Zertifikat und die dazugehörige Zertifikatskette
*[certmgr.msc]: Programm-Name des Zertifikatspeichers von Windows
*[DFN-CA Global]: Eine von der DFN-PKI betriebene, weltweit vertrauenswürdige Zertifizierungsstelle. Sie hat die Ausstellung von Zertifikaten im September 2023 eingestellt.
*[DFN-PKI]: Ein Zweig des DFN-Vereins, der mehrere Zertifizierungsstellen betreibt
*[Let's Encrypt]: Eine weltweit vertrauenswürdige Zertifizierungsstelle, die kostenlose kurzlebige (90 Tage) Serverzertifikate unter Verwendung des ACME-Protokolls ausstellt
*[ACME]: Kurz für Automatic Certificate Management Environment, ein IETF-Protokoll zum automatischen Bezug von Zertifikaten
*[mimikatz]: Eine Software zum Erforschen der Windows-Security-Subsysteme 
*[GAL]: Globale Adressliste. Das zentrale Adressbuch aller KIT-Accounts, wird im Active Directory gepflegt und regelmäßig von Outlook lokal aktualisiert.
*[PKCS12]: Dateiformat zum Speichern einer Zertifikatskette und eines privaten Schlüssels in einer einzigen verschlüsselten Datei. PKCS12-Dateien werden häufig zum Importieren und Exportieren von Zertifikaten und privaten Schlüsseln auf Windows- und macOS-Computern verwendet und haben normalerweise die Dateiendung `.p12` or `.pfx`.