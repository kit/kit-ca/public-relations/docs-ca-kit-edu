*[Sectigo]: A UK-based company that distributes digital certificates.
*[TCS]: Trusted Certificate Services, a service provided by GÉANT to obtain certificates for research institutions.
*[GÉANT]: A collaboration of European National Research and Education Networks (European National Research and Education Networks – NRENs)
*[KIT-CA]: The central certificate service provider of KIT
*[EULA]: End-user license agreement.
*[TripleDES]: An encryption method that has been considered insecure for many years.
*[SHA1]: A hashing method that has been considered cryptographically insecure for many years.
*[PKCS12]: Archive format for cryptographic objects. In this context: a private key, a certificate and the corresponding certificate chain
*[certmgr.msc]: Program name of the Windows certificate store.
*[DFN-CA Global]: A globally trusted certificate authority operated by DFN PKI. It stopped issuing certificates in September 2023.
*[DFN-PKI]: A branch of the DFN Verein which operates several certificate authorities
*[Let’s Encrypt]: A globally trusted certificate authority that issues free short lived (90 days) server certificates using the ACME protocol
*[ACME]: Short for Automatic Certificate Management Environment, an IETF protocol to automatically acquire certificates
*[mimikatz]: A tool to explore different security subsystems in Microsoft Windows
*[GAL]: Global address list. Exchange automatically creates a address list that includes every mail-enabled object in the Active Directory. Outlook regularly retrieves this list.
*[PKCS12]: File format for storing a private key with its X.509 certificate in a single, encrypted file. The filename extension for PKCS12 files is `.p12` or `.pfx`.