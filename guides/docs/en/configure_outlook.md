# Outlook (:material-microsoft-windows:)

### Certificate setup in Outlook :material-microsoft-windows:

Make sure that the following prerequisites are met:

1. You have a [valid personal certificate for your KIT email address](https://www.ca.kit.edu/p/user){ :target="_blank" }
2. You have [downloaded it in the correct format](../../../geant-tcs/en/guides/windows_wrong_crypto_p12/){ :target="_blank" }
   and [successfully installed it in the Windows certificate store](../../../geant-tcs/en/user/#install-certificate){ :target="_blank" }

Start :material-microsoft-outlook: Outlook. Select *File* at the top left and then *Options*:

![](img/configure_outlook_en_01_options.webp){ width="498" }

Select *Trust Center* on the lower left and then *Trust Center Settings…* on the right:

![](img/configure_outlook_en_02_options.webp){ width="714" }

!!! notice
    Some settings in the following steps may be enforced by your organization (e.g., for systems managed by the SCC). In this case, some settings may be grayed out and can be ignored.

Select the *Email Security* tab on the left. In the **Encrypted email** section,
make the following settings:

* ☐ Encrypt content and attachments for outgoing messages.
* ☑ Add digital signature to outgoing messages.
* ☑ Send clear text signed message when sending signed messages
* ☐ Request S/MIME recipt for all S/MIME signed messages

Then select the *Settings* button.

![](img/configure_outlook_en_03_trust_center.webp){ width="801" }

Make the following settings:

* ☑ Default Security Setting for this cryptographic message format *and*
* ☑ Default Security Setting for all cryptographic messages
* Hash Algorithm: **SHA256**
* Encryption Algorithm: **AES (256-bit)**
* ☑ Send these certificates with signed messages

![](img/configure_outlook_en_10_security_settings.webp){ width="403" }

### Select the correct certificate

If you have installed more than one certificate, you can select the correct
one under *Signing certificate* and *Encryption certificate* using the *Choose…* button:

![](../img/configure_outlook_en_20_select_certificate.webp){ width="456" }

Unfortunately, the dialog is not very helpful in finding the correct certificate.
One possible indicator is the validity period; this starts directly when the certificate
is issued. You can inspect more characteristics via *Click here to view certificate properties*:

![](img/configure_outlook_en_30_certificate_properties_general.webp){ width="404" }

In the *General* tab, apart from the validity period, nothing helpful can be found
for newer certificates (from Sectigo).

![](img/configure_outlook_en_32_certificate_properties_details_subject.webp){ width="404" }

The *Details* tab contains the email addresses under *Subject* and the name
for personal certificates.

Finish the configuration by closing all dialogs.

### Sending signed emails {: #sign-email }

Open a new email or reply to an existing email. In the *Options* tab, select the
*:material-seal-variant: Sign* button. With the settings described above, this is the new
default setting when sending emails.

![](img/configure_outlook_en_40_compose_email_signed.webp){ width="876" }

### Sending encrypted emails {: #encrypt-email }

To encrypt messages, select the *:lock: Encrypt* button in the *Options* tab.

!!! info "Encryption only possible for recipients with a valid certificate"
    To send encrypted emails, these two requirements must be met:

    1. A valid certificate exists for all receiving email addresses
    2) Outlook must know these certificates. For KIT email addresses, this is usually done automatically by the GAL.

![](img/configure_outlook_en_40_compose_email_encrypted.webp){ width="876" }

### Reference documention from Microsoft

* [Get a digital ID](https://support.microsoft.com/en-us/office/get-a-digital-id-0eaa0ab9-b8a2-4a7e-828b-9bded6370b7b){ :target="_blank" }
* [Secure messages by using a digital signature](https://support.microsoft.com/en-us/office/secure-messages-by-using-a-digital-signature-549ca2f1-a68f-4366-85fa-b3f4b5856fc6){ :target="_blank" }
* [Encrypt email messages](https://support.microsoft.com/en-us/office/encrypt-email-messages-373339cb-bf1a-4509-b296-802a39d801dc){ :target="_blank" }
* [Verify the digital signature on a signed email message](https://support.microsoft.com/en-us/office/verify-the-digital-signature-on-a-signed-email-message-21ebf9c6-3cab-48df-9559-19af76f6cbed){ :target="_blank" }
