#  Export private keys marked as not-exportable on Windows

!!! bug "Does not work on Windows 11"
    Initial testing suggests that this does not work on Windows 11. Further testing is needed and user feedback is
    appreciated.

During certificate import, Windows marks private keys as non-exportable by default:

![](img/windows_export_non-exportable_wizard_options_en.webp){ width="533" }

This will prevent everyone (attackers & legitimate users) from exporting the private key in the future.

## Prerequisites

You will need to download [mimikatz](http://blog.gentilkiwi.com/){:target="_blank"} which your endpoint security software will most likely
flag as malicious or dangerous.
Prevent this from happening by temporarily excluding your download folder from scans. Please refer to the appropriate
documentation of your endpoint security software on how to achieve this.
[Here is Microsoft's documentation for Defender](https://support.microsoft.com/en-us/windows/add-an-exclusion-to-windows-security-811816c0-4dfd-af4a-47e4-c301afe13b26){:target="_blank"}.

Download the [current release](https://github.com/gentilkiwi/mimikatz/releases){:target="_blank"} (usually called `mimikatz_trunk.zip`)
of [mimikatz](http://blog.gentilkiwi.com/){:target="_blank"} to the directory that you previously excluded from security scans.
Extract the archive.

## Exporting private keys

Open a command shell (press ++windows+r++, enter `cmd` and press ++enter++).

!!! tip "Run with Administrator rights"
    You may need a shell with elevated right. To start such a shell press ++windows+r++, enter `cmd` and
    press ++control+shift+enter++

Change into the directory that contains mimikatz and run `x64\mimikatz.exe`:

```shell
cd %HOMEPATH%\Downloads\mimikatz_trunk
.\x64\mimikatz.exe
```

Enter these commands to export all certificates and keys (see the
[mimikatz wiki](https://github.com/gentilkiwi/mimikatz/wiki/module-~-crypto#certificates){:target="_blank"} for details):

```shell
crypto::capi
privilege::debug
crypto::cng
crypto::certificates /export
```

The exported certificates and keys are in the current directory; the password is `mimikatz`.

## Cleanup

Remove mimikatz and re-enable on-access scanning. 
