# Install personal and functional certificates in macOS

Locate the [PKCS12 file](https://en.wikipedia.org/wiki/PKCS_12){:target="_blank"}
you just downloaded in Finder (should be in the Downloads folder) and
double-click to open it. Enter the password you just assigned during the download.

![](img/import_p12_macos_10.webp){ width="444" }

!!! warning "macOS < 15 claims that the password is wrong"
    macOS versions < 15 lead to the error message "Password incorrect" with both key variants available during download,
    even if the password was entered correctly.
    In this case, please upgrade to macOS 15 or follow [these instructions](https://notes.desy.de/2nvU-OFiSwOzxhflv54LNw#).

If the import process is acknowledged with this error message, you can safely ignore it.

![](img/import_p12_macos_20.webp){ width="261" }

Your certificate should now be reside in the macOS keychain.

![](img/import_p12_macos_30.webp){ width="751" }

Functional certificates have no name and are called
**Karlsruher Institut für Technologie**.

## Setting up Apple Mail

[Apple has official instructions on how to do this](https://support.apple.com/en-gb/guide/mail/mlhlp1180/16.0/mac){:target="_blank"}.

## Setting up Microsoft Outlook for Mac

These instructions do not work with New Outlook. If necessary, revert to Legacy Outlook.

![](img/macos_outlook_en_010_new_outlook_splash.webp){ width="1042" }
![](img/macos_outlook_en_020_legacy_outlook.webp){ width="293" }

Open the settings dialog:

![](img/macos_outlook_en_030_open_settings.webp){ width="293" }

Open *Accounts* under *Personal settings*:

![](img/macos_outlook_en_031_settings_all.webp){ width="995" }

On the left, select the account for which you want to use the certificate.
Then open the dialog for advanced settings dialog:

![](img/macos_outlook_en_032_settings_accounts.webp){ width="851" }

Switch to the *Security* tab.

Under *Digital signing* and *Encryption*, select the appropriate certificate.

Select **SHA-256** as the *Signing algorithm*.
Select **AES-256** as *Encryption algorithm*.

Select these:

 * Sign outgoing messages
 * Send digitally signed message as clear text
 * Include my certificates in signed messages

![](img/macos_outlook_en_033_settings_accounts_security.webp){ width="851" }