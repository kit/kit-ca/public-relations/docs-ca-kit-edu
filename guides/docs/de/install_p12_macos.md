# Personen- und Funktionszertifikat in macOS installieren

Die eben heruntergeladene [PKCS12-Datei](https://en.wikipedia.org/wiki/PKCS_12){:target="_blank"} im Finder finden
(liegt normalerweise im Downloads-Ordner) und mit Doppelklick öffnen. Geben Sie das gerade beim Download vergebene
Passwort ein.

![](img/import_p12_macos_10.webp){ width="444" }

!!! warning "macOS < 15 meldet falsches Passwort"
    macOS-Versionen < 15 führen mit beiden beim Download verfügbaren Schlüsselvarianten zur Fehlermeldung "Passwort falsch", 
    auch wenn das Passwort korrekt eingegeben wurde.
    Bitte upgraden Sie in diesem Fall auf macOS 15 oder folgen Sie [dieser Anleitung](https://notes.desy.de/2nvU-OFiSwOzxhflv54LNw#).

Sollte der Vorgang mit dieser Fehlermeldung quittiert werden, können Sie diese ignorieren.

![](img/import_p12_macos_20.webp){ width="261" }

Ihr Zertifikat sollte nun im Schlüsselbund von macOS zu finden sein.

![](img/import_p12_macos_30.webp){ width="751" }

Funktionszertifikate haben per Definition keinen Namen und heißen deshalb **Karlsruher Institut für Technologie**.

## Einrichten von Apple Mail

[Apple hat dazu eine Anleitung](https://support.apple.com/de-de/guide/mail/mlhlp1180/mac){:target="_blank"}.

## Einrichten von Microsoft Outlook für Mac

Diese Anleitung funktioniert nicht mit New Outlook. Stellen Sie gegebenenfalls auf Legacy Outlook zurück.

![](img/macos_outlook_de_010_new_outlook_splash.webp){ width="1042" }
![](img/macos_outlook_de_020_legacy_outlook.webp){ width="293" }

Öffnen Sie den Einstellungs-Dialog:

![](img/macos_outlook_de_030_open_settings.webp){ width="293" }

Öffnen Sie unter *Persönliche Einstellungen* den Unterpunkt *Konten*:

![](img/macos_outlook_de_031_settings_all.webp){ width="995" }

Wählen Sie links das Konto aus, für das Sie das Zertifikat nutzen wollen. Öffnen Sie dann den Dialog für
erweiterte Einstellungen:

![](img/macos_outlook_de_032_settings_accounts.webp){ width="851" }

Wechseln Sie in den Reiter *Sicherheit*.

Wählen Sie unter *Digital Signieren* sowie bei *Verschlüsselung* das passende Zertifikat aus.

Wählen Sie **SHA-256** als *Signaturalgorithmus*.
Wählen Sie **AES-256** als *Verschlüsselungsalgorithmus*.

Wählen Sie die Punkte

 * Ausgehende Nachrichten signieren
 * Digital signierte Nachricht als Klartext senden
 * Meine Zertifikate in signierte Nachrichten aufnehmen

![](img/macos_outlook_de_033_settings_accounts_security.webp){ width="851" }