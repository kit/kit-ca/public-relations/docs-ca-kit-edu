# Outlook (:material-microsoft-windows:)

### Zertifikat in Outlook :material-microsoft-windows: einrichten

Um diese Anleitung befolgen zu können müssen diese Voraussetzungen erfüllt sein:

1. Sie besitzen ein [gültiges Personen-Zertifikat für ihre KIT-Mailadresse](https://www.ca.kit.edu/p/nutzer){ :target="_blank" }
2. Sie haben dieses [im korrekten Format heruntergeladen](../../../geant-tcs/de/guides/windows_wrong_crypto_p12/){ :target="_blank" }
   und [erfolgreich im Windows-Zertifikatsspeicher installiert](../../../geant-tcs/de/user/#install-certificate){ :target="_blank" }

Starten Sie :material-microsoft-outlook: Outlook. Wählen Sie oben links *Datei* und dann *Optionen*:

![](img/configure_outlook_de_01_options.webp){ width="498" }

Wählen Sie links *Trust Center* und dann rechts *Einstellungen für das Trust Center…*:

![](img/configure_outlook_de_02_options.webp){ width="950" }

!!! notice
    Einige der Einstellungen in den nächsten Schritten können von Ihrer Organisation (z.B. bei vom SCC verwalteteten Systemen) erzwungen werden. Diese sind dann ausgegraut und können von Ihnen ignoriert werden.

Wählen Sie links den Reiter *E-Mail-Sicherheit*. Machen Sie im Abschnitt **Verschlüsselte E-Mail-Nachrichten** die
folgenden Einstellungen:

* ☐ Inhalt und Anlagen für ausgehende Nachrichten verschlüsseln.
* ☑ Ausgehenden Nachrichten Signatur hinzufügen.
* ☑ Signierte Nachrichten als Klartext senden 
* ☐ S/MIME-Bestätigung anfordern […]

Wählen die dann die Schaltfläche *Einstellungen*.

![](img/configure_outlook_de_03_trust_center.webp){ width="950" }

Machen Sie die folgenden Einstellungen: 

* ☑ Standardeinstellung für dieses Format kryptografischer Nachrichten *und*
* ☑ Standardsicherheitseinstellung für alle kryptografischen Nachrichten
* Hashalgorithmus: **SHA256**
* Verschlüsselungsalgorithmus: **AES (256-bit)**
* ☑ Signierten Nachrichten diese Zertifikate hinzufügen

![](img/configure_outlook_de_10_security_settings.webp){ width="435" }

### Das korrekte Zertifikat auswählen

Falls Sie mehr als ein Zertifikat installiert haben können Sie das gewünschte jeweils bei *Signaturzertifikat* und
*Verschlüsselungszertifikat* über den Button *Auswählen* selektieren:

![](../img/configure_outlook_de_20_select_certificate.webp){ width="456" }

Der Dialog ist leider nicht sehr hilfreich beim Finden des korrekten Zertifikats. Ein möglicher Indikator ist der
Gültigkeits-Zeitraum; dieser beginnt direkt bei Ausstellung. Über *Zertifikatseigenschaften anzeigen* können Sie die
weiteren Merkmale ansehen:

![](img/configure_outlook_de_30_certificate_properties_general.webp){ width="404" }

Im Reiter *Allgemein* ist außer dem Gültigkeits-Zeitraum nichts Hilfreiches zu finden bei neueren Zertifikaten.

![](img/configure_outlook_de_32_certificate_properties_details_subject.webp){ width="404" }

Im Reiter *Details* ist stehen unter *Antragsteller* die E-Mail-Adressen und bei Personenzertifikaten auch der Name.

Beenden Sie die Konfiguration, indem Sie alle Dialoge schließen.

### Signierte E-Mails verschicken {: #sign-email }

Öffnen Sie eine neue E-Mail oder antworten Sie auf eine bestehende E-Mail. Wählen Sie im Reiter *Optionen* das Feld
*:material-seal-variant: Signieren*. Bei den oben beschriebenen Einstellungen ist das die neue Standardeinstellung
für alle E-Mails.

![](img/configure_outlook_de_40_compose_email_signed.webp){ width="675" }

### Verschlüsselte E-Mails verschicken {: #encrypt-email }

Um Nachrichten zu verschlüsseln, wählen Sie im Reiter *Optionen* das Feld *:lock: Verschlüsseln*.

!!! info "Verschlüsseln nur für Empfänger mit Zertifikat"
    Um verschlüsselte E-Mails zu verschicken, müssen diese zwei Voraussetzungen gegeben sein:
    
    1. Es existiert ein gültiges Zertifikat für sämtliche Empfänger-Adressen
    2. Outlook muss diese Zertifikate kennen. Bei Personen und Mailadressen am KIT wird das in der Regel über die GAL
       gewährleistet.

![](img/configure_outlook_de_40_compose_email_encrypted.webp){ width="675" }

### Referenzdokumentation bei Microsoft

* [Anfordern einer digitalen ID](https://support.microsoft.com/de-de/office/anfordern-einer-digitalen-id-0eaa0ab9-b8a2-4a7e-828b-9bded6370b7b){ :target="_blank" }
* [Sichern von Nachrichten mithilfe einer digitalen Signatur](https://support.microsoft.com/de-de/office/sichern-von-nachrichten-mithilfe-einer-digitalen-signatur-549ca2f1-a68f-4366-85fa-b3f4b5856fc6){ :target="_blank" }
* [Verschlüsseln von E-Mail-Nachrichten](https://support.microsoft.com/de-de/office/verschl%C3%BCsseln-von-nachrichten-373339cb-bf1a-4509-b296-802a39d801dc){ :target="_blank" }
* [Überprüfen der digitalen Signatur einer signierten E-Mail-Nachricht](https://support.microsoft.com/de-de/office/%C3%BCberpr%C3%BCfen-der-digitalen-signatur-einer-signierten-e-mail-nachricht-21ebf9c6-3cab-48df-9559-19af76f6cbed){ :target="_blank" }
