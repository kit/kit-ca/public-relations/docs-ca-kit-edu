
    <!---
    <div class="kit-ca-hinweis">
    Hinweis für die Erstellung von Nutzerzertifikaten über diesen Weg:
    
    GnuTLS kann die seit einiger Zeit nötigen Attribute `givenName` und `surName`
    nicht über einzelne Key-Value-Paare in der Template-Datei erzeugen. Stattdessen
    müssen hier die einzelnen Attribute des distinguished name weggelassen und der
    komplette `DN` am Stück angegeben werden. Beispiel:
    
    <pre class="ca_code">
    <code id="yyy">
    dn = "CN = <b>Beate Beispiel</b>, givenName = <b>Beate</b>, surName = <b>Beispiel</b>, O = Karlsruhe Institute of Technology, C = DE"
    </code>
    </pre>
    
    Attribute die nicht Teil der distinguished names sind (in diesem Fall in der Regel
    `email`) müssen weiterhin als eigene Key-Value-Paare in der Template-Datei
    stehen; Beispiel:
    <pre class="ca_code">
    <code id="xxx">email = "<b>beate.beispiel@kit.edu</b>"<br>
    email = "<b>beispiel@kit.edu</b>"<br>
    email = "<b>beate-beatrice.beispiel@kit.edu</b>"
    </code>
    </pre>
    </div>
    
    Für weitergehende Einstellungen sei auf die Manpage von `certtool` oder die
    [Dokumentation auf der
    Webseite](http://www.gnutls.org/manual/html_node/certtool-Invocation.html#certtool-Invocation)
    verwiesen.
    
    -->
