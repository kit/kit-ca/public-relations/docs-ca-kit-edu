Please enter the hostname for which you want to request a certificate:
<input data-input-for="CSR_Server_CN">

First, you need to generate a key pair (private and public key).
This is done with the following command:

```
keytool -genkey \
 -alias xCSR_Server_CNx \
 -dname "CN=xCSR_Server_CNx" \
 -keyalg RSA \
 -keysize 4096 \
 -keystore xCSR_Server_Keystorex
``` 

Using this keypair, you can now create the CSR:

```
keytool -certreq \
 -alias xCSR_Server_CNx \
 -file xCSR_Server_Reqfilex \
 -keystore xCSR_Server_Keystorex
```