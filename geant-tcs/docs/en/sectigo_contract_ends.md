# End of GÉANT's contract with Sectigo

As described on the [main page](index.md), KIT-CA obtains its certificates via GÉANT TCS. 
The operation of the certification authority for GÉANT TCS has been awarded to Sectigo.
This contract was originally meant to run until April 2025, but was recently terminated unilaterally by Sectigo.
Sectigo is of the opinion that a contract end on January 10, 2025 is contractually possible.
Even though GÉANT and DFN-PKI interpret the contract differently, there is not enough time for a legal dispute.

Both GÉANT and DFN-PKI are working hard to find a new CA operator as soon as possible in order to keep any gap in certificate issuance as small as possible.
However, due to the short notice of the termination, it will probably not be possible to avoid such a gap.

**KIT-CA will therefore no longer be able to issue certificates via Sectigo from January 10, 2025.**

!!! warning "Transitional contract signed"
    On December 4, 2024, DFN-PKI announced that a transitional contract had been signed with the Certificate Authority HARICA.
    We hope to be able to use this to issue certificates after the contract with Sectigo ends.
    The exact process is still unknown, but will definitely cause more manual work on our side.
    Please continue to support us by renewing expiring certificates now as instructed in our email to affected users.

!!! quote "I want to know *everything*!"
    Everything we currently know is documented by DFN-PKI in a [blog article](https://doku.tid.dfn.de/de:dfnpki:tcsfaq:aktuellesituation).
    We do not have more information than the DFN-PKI.
    Please refrain from inquiries that do not concern the specific measures taken by KIT-CA.

### What is KIT-CA's strategy to mitigate the situation? {data-toc-label="Strategy of KIT-CA"}

In order to provide as much information as possible and to keep the number of affected users as small as possible, we plan to provide information in several stages.

- Inform IT officers about the situation by referring them to this page.
- November 27, 2024: Presentation at the ITB-Versammlung.
- Information email to contacts of server certificates and all KIT members who have a mail certificate that expires within a year.
- December 4, 2024: Offering an information and discussion meeting via Teams to answer questions and concerns. This will be exclusive to IT officers. If you have questions as an end user, please pass them on to the IT officer of your organizational unit.

### What happens to my certificates after the contract ends? {data-toc-label="Certificates after contract end"}

To the best of our knowledge, all issued and valid certificates will remain valid.
According to the certification guideline and the terms of the contract, revocation after the end of the contract is not permitted.
Sectigo would put themselves into distrust with global trust anchors like operating systems and browsers, which gives us confidence that they won't revoke any valid certificates.

### My mail certificate is about to expire, what should I do? {data-toc-label="Expiring mail certificates"}

You are probably reading this section because you have received an email from us informing you that you should renew your certificate.
Please follow our [instructions](https://ca.kit.edu/p/nutzer) to obtain a new certificate.

Once issued, the new certificate will in most cases be used immediately for encrypted emails sent to you - make sure you set it up correctly.
Both variants will continue to work until the old certificate expires (please also refer to the section "[What happens to my certificates after the contract ends?](sectigo_contract_ends.md#what-happens-to-my-certificates-after-the-contract-end)").

If several certificates are mentioned in your mail, it is sufficient to request **one** new certificate.

More details about the concepts behind certificates can be found at [concepts](concepts.md).

### Someone new will join us in Q1/Q2 2025 who absolutely needs a mail certificate for their work! {data-toc-label="Mail certificate for new employees in Q1/Q2"}

If there is sufficient justification, we can issue a function certificate for accounts that do not yet exist, as the email addresses assigned to the account are predictable.
However, the process is time-consuming for us.
Please email us at [ca@kit.edu](mailto:ca@kit.edu) with sufficient justification as to why a certificate is absolutely necessary.

If you only read this section after January 10, 2025, please email us at [ca@kit.edu](mailto:ca@kit.edu).

### My server certificate expires in Q1/Q2 2025, what should I do? {data-toc-label="Expiring server certificates"}

We will renew all server certificates currently issued via Sectigo in time.
The private key of your current certificate will be reused (contrary to our usual recommendations), you will only have to swap the certificate.
You will automatically receive the new certificate by e-mail to the e-mail address you provided when you first applied.

### I have to set up an important service in Q1/Q2 2025 and need server certificates! {data-toc-label="Server certificate for new service in Q1/Q2"}

Almost all use cases for server certificates can be accomodated for with certificates from Let's Encrypt.
This also applies to mutual authentication and systems that are not accessible from the Internet themselves.
You can find instructions on how to set this up in our [ACME4NETVS documentation](https://docs.ca.kit.edu/acme4netvs).

If you need more time to switch to using Let's Encrypt, we have revived our [Transition service](https://transition.ca.kit.edu/).
This makes it possible to obtain certificates from Let's Encrypt without setting up an ACME client.
The issued certificate is, as usual with Let's Encrypt, only valid for 3 months.

If you still have a use case that absolutely requires long-running or organization validated certificates, please follow our [Instructions for applying for server certificates](server.md) with the domain names you likely need **before the KIT closes over Christmas and New Year's Eve**.

If you only read this section after January 10, 2025 and none of the other solutions work for you, please email us at [ca@kit.edu](mailto:ca@kit.edu).
