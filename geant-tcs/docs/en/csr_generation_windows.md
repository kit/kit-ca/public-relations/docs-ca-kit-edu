Please enter the hostname for which you want to request a certificate:
<input data-input-for="CSR_Server_CN">

!!! important "Wichtig"
    Noone working at KIT-CA uses this way of CSR generation, therefore we have next to no experience with it.
    If there are any problems, it might help to check out the official documentation ([certreq.exe](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/certreq_1), [certutil.exe](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/certutil){:target="_blank"}).

On Windows, you can also create CSRs using the command line.
In order to do so, you first need to create a file named **xCSR_Server_Templatefilex** with the following content:

```
[NewRequest]
Exportable = TRUE
KeyLength = 4096
HashAlgorithm = sha256
MachineKeySet = TRUE
Subject = "CN=xCSR_Server_CNx"
RequestType = PKCS10
UserProtected = FALSE
```

In order to add [Subject Alternative Names (SANs)](http://en.wikipedia.org/wiki/SubjectAltName){:target="_blank"}, add an additional section `[Extensions]` to the previously created `xCSR_Server_Templatefilex` with the required hostnames (each between `_continue_ = "DNS=` und `&"`):

```
[Extensions]
2.5.29.17 = "{text}"
_continue_ = "DNS=<b>weiterer-hostname.ifmb.kit.edu</b>&"
_continue_ = "DNS=<b>noch-ein-hostname.ifmb.kit.edu</b>&"
```

Die CSR can now be generated with the following command.
This overrides and previously existing file named `xCSR_Server_Reqfilex` with the new file.

```
certreq -new xCSR_Server_Templatefilex xCSR_Server_Reqfilex
```

You can look at the generated CSR using:

```
certutil -dump xCSR_Server_Reqfilex
```
