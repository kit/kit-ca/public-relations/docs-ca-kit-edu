# Certificate Revocation

It is sometimes necessary to prevent certificates from being used before their regular expiration date.
Since an actively used certificate was potentially occupied onto countless computers,
certificates cannot simply be _deleted_. Instead, certificates are _revoked_.
These certificates are stored on central [revocation lists](https://de.wikipedia.org/wiki/Zertifikatsperrliste){:target="_blank"}
or [revocation servers](https://de.wikipedia.org/wiki/Online_Certificate_Status_Protocol){:target="_blank"} of the CA provider.
Clients such as Outlook and Thunderbird use these to regularly check whether certificates are still valid.

!!! warning
    Revocations cannot be reverted! It is therefore important to exercise appropriate caution when revoking certificates.

The revocation status of a certificate has no influence on its cryptographic use. As long as you are
in possession of the matching private key, it will still be possible to decrypt encrypted emails for
revoked certificates in the future. The following therefore also applies here: never delete
certificates for which you still have or expect encrypted e-mails.

## Revoke certificates at KIT

Unfortunately, there is currently no self-service for certificate revocation.

To revoke certificates, have an _authorized person_ send a signed e-mail to
[ca@kit.edu](mailto:ca@kit.edu?subject=Please%20revoke%20certificate%28s%29&body=Hi%21%0A%0APlease%20revoke%20the%20following%20certificate%0A%0A%2A%20Email%3A%20CHANGEME%40kit.edu%0A%2A%20Serial%20Number%3A%200x1234567890abcdef00000001%0A%2A%20Reason%3A%20%E2%80%A6)
with a subject of `Bitte Zertifikat(e) sperren`, listing all certificates to be revoked in the mail body.
Please specify one of the associated e-mail addresses and the serial number for each certificate. You can find these either in the certificate store of your operating system (`certmgr.msc` on Windows and
_Keychain management_ in macOS) or in the [Certificate search of the KIT-CA](https://search.ca.kit.edu/){:target="_blank"}.

An _authorized person_ is either the original requester or an appropriate IT officer (ITB).
In the case of function certificates, all persons who have access to the relevant mailbox also qualify.