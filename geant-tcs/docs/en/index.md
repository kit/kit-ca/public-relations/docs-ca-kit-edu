# GÉANT Trusted Certificate Services

Until 31.08.2023, KIT-CA used DFN-CA Global, provided by DFN-PKI, for issuance of certificates to our users.
Since the DFN-PKI does not have sufficient staff capacity to maintain the regularly required changes for globally trusted certification authorities, DFN-CA Global was discontinued.

GÉANT Trusted Certificate Services (TCS) is available as its successor.
GÉANT is the collaboration of the European National Research and Education Networks (NRENs), offering various services and infrastructure for these national organisations. 
One of these services is TCS.
Via the German Research Network (DFN), the NREN in Germany, KIT-CA is granted access to TCS and can obtain certificates for users and servers. 
    
TCS is operated by a Certification Authority Operator, currently Sectigo.

!!! warning "GÉANT TCS: End of contract with Sectigo"
    The contract of GÉANT TCS with Sectigo for operations of the certification authority ends on 10.01.2025.
    KIT-CA will temporarily not be able to issue new certificates.
    All information about this can be found in our [article](/geant-tcs/en/sectigo_contract_ends).

The CA Operator is re-contracted every 5 years, so there may be regular changes.
This means that certificate application process could also change every 5 years, leading to confusion on the user side every time.
As KIT-CA, it is important to us that the application for certificates at KIT is transparent for all users and that the process remains as consistent as possible.
Therefore, we are currently building our own portal for applying for certificates which will stay the same even if the CA Operator changes.

The first version of the CA-Portal, which for now partly uses the process of Sectigo, is available on [https://portal.ca.kit.edu](https://portal.ca.kit.edu){:target="_blank"}.
Documentation for the whole process is available here:

* [Email Certificates](user.md)
* [Server Certificates](server.md)
