# Generating certificate signing requests (`.csr`/`.req`) for server certificates

Certificate Signing Requests (CSRs) can be created with multiple tools.
On this page, we describe the most commonly used ones - GnuTLS, OpenSSL, Java Keystore and Windows certutil.

=== "OpenSSL"
    
    {% macro openssl() %}{% include 'csr_generation_openssl.md' %}{% endmacro %}
    {{ openssl() | indent }}

=== "GnuTLS"

    {% macro gnutls() %}{% include 'csr_generation_gnutls.md' %}{% endmacro %}
    {{ gnutls() | indent }}

=== "Java Keystore"

    {% macro java() %}{% include 'csr_generation_java.md' %}{% endmacro %}
    {{ java() | indent }}

=== "certutil (Windows)"
    
    {% macro windows() %}{% include 'csr_generation_windows.md' %}{% endmacro %}
    {{ windows() | indent }}

