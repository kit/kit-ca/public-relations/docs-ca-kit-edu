Please enter the hostname for which you want to request a certificate:
<input data-input-for="CSR_Server_CN">

Using [OpenSSL](https://www.openssl.org/){:target="_blank"}, you can generate a key and the related CSR using the command line.
`FQHN` is the [Fully Qualified Host Name](http://de.wikipedia.org/wiki/Fully-Qualified_Host_Name){:target="_blank"}.

```
openssl req -newkey rsa:4096 \
 -out xCSR_Server_Reqfilex \
 -keyout xCSR_Server_Keyfilex \
 -nodes \
 -subj '/CN=xCSR_Server_CNx'
```

The parameter `-nodes` is used in order to save the private key without encryption.
This is useful for most server applications.
Otherwise, you will need to enter the passwort on every start of the application or at worst render the application unable to start.
In order to save the private key encrypted with a password, just leave out the `-nodes` parameter.


In order to add [Subject Alternative Names (SANs)](http://en.wikipedia.org/wiki/SubjectAltName){:target="_blank"}, the following command can be used.
Make sure to change the hostnames to the ones you actually need.
Every hostname must be prefixed with `DNS:`, multiple entries must be seperated by a `,`.

```
openssl req -newkey rsa:4096 \
 -out xCSR_Server_Reqfilex \
 -keyout xCSR_Server_Keyfilex \
 -nodes \
 -subj '/CN=xCSR_Server_CNx' \
 -addext 'subjectAltName = DNS:weiterer-hostname.ifmb.kit.edu,DNS:noch-ein-hostname.ifmb.kit.edu'
```
