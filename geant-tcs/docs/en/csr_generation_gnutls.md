Please enter the hostname for which you want to request a certificate:
<input data-input-for="CSR_Server_CN">

With [GnuTLS](http://www.gnutls.org/){:target="_blank"}, you can generate a key and the related CSR using the command line.
`FQHN` is the [Fully Qualified Host Name](http://de.wikipedia.org/wiki/Fully-Qualified_Host_Name){:target="_blank"}.

Use the following command in order to generate the private key:

```
certtool --generate-privkey --outfile xCSR_Server_Keyfilex
```

Create a template file named `xCSR_Server_Templatefilex` containing the following content:

```
organization = "Karlsruhe Institute of Technology"
locality = "Karlsruhe"
state = "Baden-Wuerttemberg"
country = DE
cn = "xCSR_Server_CNx"
dns_name = "xCSR_Server_CNx"
```

In order to add [Subject Alternative Names (SANs)](http://en.wikipedia.org/wiki/SubjectAltName){:target="_blank"}, add additional `dns_name` lines to the previously created template file `xCSR_Server_Templatefilex`:

```
dns_name = "additional-hostname.ifmb.kit.edu"
dns_name = "another-hostname.ifmb.kit.edu"
```

You can now create the CSR:

```
certtool --generate-request --hash SHA256 --no-text \
 --load-privkey xCSR_Server_Keyfilex \
 --template xCSR_Server_Templatefilex \
 --outfile xCSR_Server_Reqfilex
```	

