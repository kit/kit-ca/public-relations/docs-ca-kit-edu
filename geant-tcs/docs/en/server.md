# Server certificates with GÉANT TCS

We recommend to automate issuance of server certificates using [Let's Encrypt with acme4netvs](https://docs.ca.kit.edu/acme4netvs/en/).

If this is not an option for you (e.g. because the server can't connect to the internet), you can also request certificates using GÉANT TCS. 

### Available Certificate Profiles

The following certificate profiles are available via TCS:

| Profile           | Certificate Term | Available Key Types                                                                             |
|-------------------|------------------|-------------------------------------------------------------------------------------------------|
| OV Multi-Domain   | 365 days         | Elliptic Curve (EC) P-256 (256 bit), P-384 (384 bit), RSA 2048 bit, 3072 bit, 4096 bit          |
| IGTF Multi-Domain | 395 days         | Elliptic Curve (EC) P-256 (256 bit), P-384 (384 bit) RSA 2048 Bit, 3072 Bit, 4096 Bit, 8192 Bit |

!!! warning
    IGTF Multi-Domain is specially designed for the grid environment and reserved for this use. 
    The authorization for this is checked by us before issuance.

### Request Process

1. [Create a Certificate Signing Request (CSR)](csr_generation.md) with all domains required in the certificate as Subject Alternative Name (SAN). The Common Name (CN) can be any domain. Make sure that the Private Key corresponds to one of the key types listed above, otherwise the certificate cannot be issued.
2. Send the CSR with by mail to [ca@kit.edu](mailto:ca@kit.edu?subject=%5BTCS%20Certificate%20Request%5D%20Request%20for%20a%20server%20certificate&body=Hello%2C%0A%0APlease%20issue%20a%20server%20certificate%20for%20the%20attached%20CSR.%0A%0A%2A%20This%20mail%20is%20signed%0A%2A%20I%20am%20authorized%20to%20receive%20certificates%20for%20the%20server%20in%20the%20following%20position%3A%20Reason%0A%2A%20The%20certificate%20and%20notifications%20will%20be%20sent%20to%3A%20Mail%20address%0A%0AUnfortunately%2C%20Let%27s%20Encrypt%20cannot%20be%20used%20because%3A%0AReason%0A%0ABest%20regards%0AName). The mail must meet the following conditions:
    * The mail must be S/MIME signed by a person authorized for the domain.
    * [TCS Certificate Request] at the beginning of the subject line.
    * The mail address that should receive the completed certificate and notifications about it must be explicitly specified in the mail body.
    * Optional: We appreciate a short explanation why Let's Encrypt is (currently) not an option for you.
3. We issue the certificate after checking the permissions.
4. You will receive a download link to the certificate and the certificate chain by mail from support@cert-manager.com.

!!! note
    If the system previously used a certificate from DFN-CA Global and the operated software does not use the certificate store of the operating system, the certificate chain must be imported. A download link for the certificate chain is part of the certificate mail. This is often the case with Java software, for example.

