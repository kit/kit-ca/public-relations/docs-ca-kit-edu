# Repairing unusable certificate files from Sectigo

!!! warning "These instructions are obsolete!"
    Sectigo changed this part of its process on 14.9.2024. Since then, the setting “Secure AES256-SHA256” is the correct option for almost all systems.
    These instructions are only provided here for users of old certificates.

[This guide is currently only available in German](../../../de/guides/windows_wrong_crypto_p12/).
