# Interim workflow for personal certificates

This page describes the current (interim) process to get a personal certificate to sign
documents and sign/decrypt emails.

This service uses the [GÉANT TCS project](https://security.geant.org/trusted-certificate-services/){:target="_blank"}
which itself uses the services of [Sectigo](https://www.sectigo.com/){:target="_blank"}.

<a name="request-certificate"></a>
## Request a new certificate

!!! danger "No certificate issuance possible"
    Sectigo, the operator of the certification authority that issues our certificates, removed access for us on January 10, 2025.
    Therefore, no certificates can be requested at the moment until the new operator is properly integrated.
    This may take a few weeks, we ask for your understanding and are sorry for any inconvenience caused.

Log in to the [CA-Portal](https://portal.ca.kit.edu){:target="_blank"} with the account owning the
email addresses you want a certificate for. Use the private mode of your browser if
you are already logged in with another account.

### Choose Certificate Type

Personal certificates are bound to an individual person and are only issued for the email addresses of the corresponding
KIT account. Before applying, according to policy, you must identify yourself in person. An identification is
currently valid for ten years. Personal certificates can be recognized by the fact that the "Certificate Name"
(correctly: Common Name) includes the natural name of the owner.

Functional/group certificates are functionally equivalent to personal certificates. They are not bound to an individual person, but may be shared with all users of the associated email mailboxes. Functional certificates
contain only the associated email addresses and no "Certificate Name" (Common Name). You can also use a functional certificate for personal email addresses instead of personal certificates. In this case, identification is not necessary.

!!! danger "Mailing lists"
    Functional certificates cannot be issued for mailing lists (@lists.kit.edu and @listserv.dfn.de) with the current process from Sectigo, as the challenge for this would be sent directly to the list members. If you need such a certificate please email us at [ca@kit.edu](mailto:ca@kit.edu?subject=TCS%20Certificate%20Request%20for%20mailinglist) to coordinate the process.

Choose `Request` for the desired certificate type:

![](img/ca-portal_en_request_01.webp){ width="796" }

### Personal Certificate

If you do not have a valid identification, you cannot apply for a personal certificate:

![](img/ca-portal_en_request_02_personal_no-ident.webp){ width="796" }

Choose one of the described options.

If you have valid identification, you can select which email addresses should be added to
the certificate:

![](img/ca-portal_en_request_10_personal_select_email.webp){ width="800" }

Now, all data added to the certificate is displayed for proofreading. Choose `Submit` if everything
is correct:

![](img/ca-portal_en_request_20_personal_submit.webp){ width="800" }

Follow the instructions in the browser.

### Functional Certificate

Enter all email addresses that should be added to the certificate. Pay attention to the hints given in the portal!

![](img/ca-portal_en_request_10_functional_add_emails.webp){ width="800" }

Now, all data that will be added to the certificate is displayed for proofreading. Choose `Submit` if everything
is correct:

![](img/ca-portal_en_request_20_functional_submit.webp){ width="800" }

<a name="backup-p12"></a>
## Create a Backup

Backup your certificate/key file and the corresponding password. We strongly urge you to do it **now**, postponing
usually results in never making backups at all.

You will need every key/certificate pair (usually the `.p12` file) for which you have ever received encrypted emails until
you quit working at KIT.

Secure both the certificate file and the password in a way that you can still safely find and read them in the far
future. For security reasons, it is advisable to store both separately from each other.

??? note ":construction: Work in Progress"
    Unfortunately, this section is still somewhat rudimentary & incomplete.

<a name="install-certificate"></a>
## Install the issued certificate

The PKCS12 file you just downloaded can usually be imported by double-clicking
(Windows, macOS) or simply importing it in the application's settings dialog (Thunderbird).

Note to Windows user: During import, set the option _Mark key as exportable_. This allows you to copy the certificate
and private key from this computer to the new device when switching computers:

![](img/windows_11_certificate_import_allow-export_en.webp){ width="531" }

## E-Mail Client Configuration

* [Outlook in Windows](/guides/en/configure_outlook/)
* [macOS & Apple Mail](/guides/en/install_p12_macos/)
* [Thunderbird (links to guide from Univcersity of Heidelberg, german only)](https://www.urz.uni-heidelberg.de/de/support/anleitungen/import-der-smime-zertifikate-in-thunderbird){:target="_blank"}