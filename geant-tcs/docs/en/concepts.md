# Relevant concepts

This page describes some basic concepts needed to properly  handle [X509 certificates](https://de.wikipedia.org/wiki/X.509){:target="_blank"}.
In the remaining instructions on this site, we assume that all readers have read and understood this page.

!!! tip
    The central message(s) of each section are summarized in boxes like this one.

## Principal structure of X509 certificates

X509 certificates are part of a [asymmetric cryptography system](https://en.wikipedia.org/wiki/Public-key_cryptography){:target="_blank"}.

Asymmetric cryptography uses related pairs of related keys. Each key pair consists of a __public key__ and a corresponding
__private key__. The actual __certificate__ is the _public_ key combined with the signature of a certification authority
and a set of attributes (name, validity period, purpose, serial number, …).

E-mail senders encrypt e-mails **with** the recipient's **public** key. Recipients then **decrypt** them with their
**secret key**. E-mails are **signed** with the **secret key** and validated by the recipient using the sender's
**public key**.

As the name suggests, the **secret** key must be kept secret. Anyone who possesses this key can decrypt
encrypted messages and sign documents and messages on behalf of the certificate holder. This is why all procedures for
generating a secret key are designed to ensure that the requesting person holds all copies the private key.

## Backups

Because the private keys are __only__ held by the certificate owner, they must themselves create secure backups of
__all__ private keys. These backups must be accessible for as long as anyone still wants to decrypt/read the
corresponding encrypted e-mails. This means that every certificate holders needs backups of all certificates/private keys
until they leave KIT.
Make sure that you will also be able to know/retrieve the passwords of the `.p12` files many years from now.

!!! tip "Backups"
    Make secure backups/copies of the private keys of __all__ certificates until the end of your employment relationship
    with KIT. The private keys are normally contained in files with the extension `.p12` or `.pfx`.
    Also remember to back up all associated passwords.

## Always sign e-mails

To encrypt e-mails, the sender needs the certificates of all recipients in advance. Users of Outlook at KIT
receive these via the GAL ("Global Address List"). Users of other e-mail clients save these automatically from
signed e-mails.

!!! tip "Always sign e-mails"
    Always sign your e-mails. This helps to distribute your certificate to all potential correspondence partners.

## Only one certificate per identity/entity

!!! warning "Function and group certificates"
    If you share an e-mail address with several people, all the people concerned must share __a single__ certificate.
    The person requesting the certificate must therefore distribute it to the rest in a secure manner.

When sending encrypted messages, the sender's e-mail client selects _exactly one_ certificate for each recipient. If
the e-mail client knows of several valid certificates, some (often undocumented) rules are used to select one;
usually either the newest or the one with the longest validity. Therefore, there should only ever be __one__
valid certificate per identity.

!!! tip "Always revoke lost certificates"
    If you no longer have access to the corresponding secret key for a certificate, revoke it
    immediately. This will prevent reception of emails that cannot be decrypted.
    
    See [here](https://www.ca.kit.edu/p/instructions/user/revocation) for a guide on how to revoke certificates.

If you want to sign or decrypt e-mails on more than one device, you must securely transfer your private key and
the certificate (in practice: the `.p12` file) to all devices in a secure way and set them up there.
<!-- TODO: Write and link instructions__. -->

!!! tip "Distribute certificate to all end devices"
    Copy your current certificate to all relevant end devices.
<!-- TODO: Link to instructions -->