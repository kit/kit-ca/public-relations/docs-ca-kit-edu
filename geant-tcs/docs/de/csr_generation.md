# CSR-Generierung für Serverzertifikate

Zertifikatsantragsdateien (Certificate Signing Requests, CSRs) können mit verschiedenen Werkzeugen erstellt werden.
Hier werden die verbreitetsten Werkzeuge beschrieben - GnuTLS, OpenSSL, Java Keystore and Windows certutil - beschrieben.

=== "OpenSSL"
    
    {% macro openssl() %}{% include 'csr_generation_openssl.md' %}{% endmacro %}
    {{ openssl() | indent }}

=== "GnuTLS"

    {% macro gnutls() %}{% include 'csr_generation_gnutls.md' %}{% endmacro %}
    {{ gnutls() | indent }}

=== "Java Keystore"

    {% macro java() %}{% include 'csr_generation_java.md' %}{% endmacro %}
    {{ java() | indent }}

=== "certutil (Windows)"
    
    {% macro windows() %}{% include 'csr_generation_windows.md' %}{% endmacro %}
    {{ windows() | indent }}

