# Relevante Konzepte

Im Folgenden werden einige grundlegende Dinge zum Umgang mit [X509-Zertifikaten](https://de.wikipedia.org/wiki/X.509){:target="_blank"}
beschrieben. Wir gehen in den restlichen Anleitungen davon aus, dass alle Leser:innen diese Seite gelesen und verstanden
haben.

!!! tip
    Die zentrale Kernaussage(n) jedes Abschnitts sind in Boxen wie dieser zusammengefasst.

## Genereller Aufbau

X509-Zertifikate sind Bestandteil eines [asymmetrischen](https://de.wikipedia.org/wiki/Asymmetrisches_Kryptosystem){:target="_blank"}
[Kryptosystems](https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Onlinekommunikation/Verschluesselt-kommunizieren/Arten-der-Verschluesselung/arten-der-verschluesselung.html){:target="_blank"}.

Es gibt also für jedes Zertifikat ein Paar aus **geheimen Schlüssel** und **öffentlichen Schlüssel**. Das eigentliche
**Zertifikat** ist der **öffentliche** Schlüssel zusammen mit der Signatur einer Zertifizierungsstelle sowie einem Satz
an Attributen (Name, Gültigkeitszeitraum, Einsatzzwecke, Seriennummer, …).

E-Mail-Absender verschlüsseln E-Mail **mit** einen **öffentlichen** Schlüssel. Empfänger **entschlüsseln** diese dann mit
dem **geheimen Schlüssel**. Mails werden mit dem **geheimen Schlüssel** unterschrieben („signiert“) und dem
**öffentlichen** Schlüssel geprüft („validiert“).

Wie der Name suggeriert, muss der **geheime** Schlüssel geheim gehalten werden. Jeder, der diesen besitzt, kann
damit sowohl verschlüsselte Nachrichten entschlüsseln als auch im Namen des Zertifikatsinhabers Dokumente und Nachrichten
signieren. Deswegen sind alle Verfahren zur Erzeugung eines geheimen Schlüssels darauf ausgelegt, dass nur die
beantragende Person diesen selber erzeugt und dabei keine Kopien für Dritte erzeugt werden.

## Backups

Daraus folgt, dass die privaten Schlüssel __nur__ beim Zertifikats-Inhaber existieren. Deswegen __müssen__ diese sich
auch selber darum kümmern, sichere Backups __aller__ privaten Schlüssel zu erstellen. Und zwar so lange, wie irgendjemand
die dafür verschlüsselten E-Mails noch lesen will. Das ist potenziell bis zum Ende des Dienstverhältnisses mit dem KIT!
Sorgen Sie dafür, dass Sie auch die Passwörter der `.p12`-Dateien noch in vielen Jahren wissen/wiederfinden können.

!!! tip "Backups"
    Machen Sie sichere Backups/Kopien der privaten Schlüssel __sämtlicher__ Zertifikate bis zum Ende ihres Dienstverhältnisses
    mit dem KIT. Die privaten Schlüssel sind normalerweise in Dateien mit der Endung `.p12` oder `.pfx` enthalten.
    Denken Sie auch daran, die zugehörigen Passwörter zu sichern.

## E-Mails immer Signieren

Um E-Mails zu verschlüsseln, benötigt der Absender vorab die Zertifikate sämtlicher Empfänger. Nutzer von Outlook am KIT
erhalten diese über die GAL („Globale Adressliste“). Nutzer anderes E-Mail-Clients speichern diese automatisch aus
signierten E-Mails.

!!! tip "E-Mails immer Signieren"
    Signieren Sie alle E-Mails. Damit stellen Sie sicher, das sämtliche ihrer Korrespondenz-Partner ihr Zertifikat
    haben und dass Ihre E-Mails Ihnen klar zugeordnet werden können.

## Nur ein Zertifikat pro Identität

!!! warning "Funktions- und Gruppenzertifikate"
    Wenn Sie sich eine E-Mail-Adresse mit mehreren Personen teilen, müssen sich alle betroffenen Personen __dieses eine__
    Zertifikat teilen. Die beantragende Person muss dieses also nach Erhalt auf sicherem Weg an den Rest verteilen.

Beim verschlüsselten Versenden wählt der E-Mail-Client des Absenders für jeden Empfänger genau ein Zertifikat aus. Wenn
der E-Mail-Client mehrere gültige Zertifikate kennt wird nach irgendwelchen (oft nicht dokumentierten) Regeln irgendeines
ausgewählt; normalerweise entweder das Neueste oder das mit der längsten Gültigkeit. Deswegen sollte es immer nur **ein**
gültiges Zertifikat pro Identität geben.

!!! tip "Verlorene Zertifikate immer sperren"
    Wenn Sie für ein Zertifikat keinen Zugriff (mehr) auf den zugehörigen geheimen Schlüssel haben, sperren Sie dieses
    umgehend. Dann bekommen Sie keine nicht-entschlüsselbaren E-Mails.
    
    Der Prozess zum Sperren von Zertifikaten ist [hier](https://www.ca.kit.edu/p/anleitungen/user/sperren) beschrieben.

Wenn Sie auf mehr als einem Endgerät Mails Signieren oder Entschlüsseln wollen, müssen Sie ihren privaten Schlüssel und
das Zertifikat (in der Praxis: die `.p12`-Datei) auf sicherem Weg auf alle Geräte kopieren und dort einrichten.
<!-- TODO: Anleitungen schreiben und verlinken__. -->


!!! tip "Zertifikat auf alle Endgeräte verteilen"
    Kopieren Sie ihr aktuelles Zertifikat auf alle relevanten Endgeräte.
<!-- TODO: Link auf Anleitung -->