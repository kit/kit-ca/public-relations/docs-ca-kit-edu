Bitte geben Sie den Hostnamen an, für den Sie ein Zertifikat beantragen müssen:  
<input data-input-for="CSR_Server_CN">

Mit [GnuTLS](http://www.gnutls.org/){:target="_blank"} können Schlüssel und Antragsdatei über die Kommandozeile generiert werden.
`FQHN` ist der [vollqualifizierte Hostname](http://de.wikipedia.org/wiki/Fully-Qualified_Host_Name){:target="_blank"}.

Verwenden Sie folgenden Befehl, um den privaten Schlüssel zu generieren:

```
certtool --generate-privkey --outfile xCSR_Server_Keyfilex
```
 
Legen Sie eine Template-Datei namens `xCSR_Server_Templatefilex` mit folgendem Inhalt:

```
organization = "Karlsruhe Institute of Technology"
locality = "Karlsruhe"
state = "Baden-Wuerttemberg"
country = DE
# CN; bitte einen vollqualifizierten Hostnamen verwenden
cn = "xCSR_Server_CNx"
dns_name = "xCSR_Server_CNx"
```

Um [Subject Alternative Names (SANs)](http://en.wikipedia.org/wiki/SubjectAltName){:target="_blank"} zu setzen, fügen Sie diese als `dns_name` der zuvor angelegten Datei `xCSR_Server_Templatefilex` an:

```
dns_name = "weiterer-hostname.ifmb.kit.edu"
dns_name = "noch-ein-hostname.ifmb.kit.edu"
```

Erzeugen Sie nun die Antragsdatei:

```
certtool --generate-request --hash SHA256 --no-text \
 --load-privkey xCSR_Server_Keyfilex \
 --template xCSR_Server_Templatefilex \
 --outfile xCSR_Server_Reqfilex
```	

