# Manueller Weg zur Beantragung von Personen- und Funktionszertifikaten

Diese Seite beschreibt den aktuellen (übergangs-)Weg zur Beantragung von Zertifikaten
zum Signieren und Entschlüsseln von E-Mails.

Die eigentliche Zertifikats-Erstellung nutzt das [GÉANT TCS Projekt](https://security.geant.org/trusted-certificate-services/){:target="_blank"},
die wiederum auf [Sectigo](https://www.sectigo.com/){:target="_blank"} als Dienstleister zurückgreifen.

<a name="request-certificate"></a>
## Zertifikat beantragen

!!! danger "Keine Zertifikatsausstellung möglich"
    Sectigo, der Betreiber der Zertifizierungsstelle, über die wir Zertifikate erhalten, hat am 10.01.2025 unseren Zugang gesperrt.
    Es können deshalb gerade keine Zertifikate beantragt werden, bis der neue Betreiber vollständig in unsere Prozesse integriert ist.
    Dies kann einige Wochen dauern, wir bitten um Ihr Verständnis und entschuldigen uns für die Umstände.

Loggen Sie sich im [CA-Portal](https://portal.ca.kit.edu){:target="_blank"} mit dem Account ein, für dessen
E-Mail-Adressen Sie ein Zertifikat haben wollen. Nutzen Sie gegebenenfalls den privaten Modus ihres Browsers, wenn
Sie bereits mit einem anderen Account eingeloggt sind.

### Zertifikatstyp wählen

Personenzertifikate sind an eine natürliche Person gebunden und werden nur für die E-Mail-Adressen des dazugehörenden
KIT-Accounts ausgestellt. Vor dem Beantragen muss gemäß Policy eine persönliche Identifizierung stattfinden. Diese ist
aktuell zehn Jahre gültig. Man erkennt Personenzertifikate daran, dass im „Zertifikatsnamen“ (korrekt: Common Name)
der natürliche Name des Besitzenden steht. 

Funktions-/Gruppenzertifikate sind funktional äquivalent zu Personenzertifikaten. Sie sind nicht an eine einzelne Person
gebunden, sondern dürfen mit alle Nutzenden der zugehörigen E-Mail-Postfächer geteilt werden. Funktionszertifikate
enthalten nur die zugehörigen E-Mail-Adressen und keinen „Zertifikatsnamen“ (Common Name). Auch für persönliche
E-Mail-Adressen können Funktions- statt Personenzertifikate genutzt werden. In diesem Fall entfällt die Identifikation.

!!! danger "Mailinglisten"
    Funktionszertifikate können mit dem aktuellen Prozess von Sectigo nicht für Mailinglisten (@lists.kit.edu und
    @listserv.dfn.de) ausgestellt werden; da die Challenge dafür direkt an die Listenmitglieder versendet werden würde.
    Falls Sie ein solches Zertifikat benötigen, schicken Sie uns bitte eine E-Mail an [ca@kit.edu](mailto:ca@kit.edu?subject=TCS%20Certificate%20Request%20for%20mailinglist) zur Koordination des Prozesses.

Wählen Sie `Beantragen` beim gewünschten Zertifikatstyp aus:

![](img/ca-portal_de_request_01.webp){ width="796" }

### Personenzertifikat

Falls Sie keine gültige Identifizierung haben, können Sie kein Personenzertifikat beantragen:

![](img/ca-portal_de_request_02_personal_no-ident.webp){ width="796" }

Wählen Sie eine der dort beschrieben Optionen.

Wenn Sie eine gültige Identifizierung haben, können Sie im nächsten Schritt auswählen, welche E-Mail-Adressen ins
Zertifikat geschrieben werden sollen:

![](img/ca-portal_de_request_10_personal_select_email.webp){ width="800" }

Jetzt werden nochmal alle Daten angezeigt, die ins Zertifikat geschrieben werden. Wählen Sie `Absenden`, wenn alles
korrekt ist:

![](img/ca-portal_de_request_20_personal_submit.webp){ width="800" }

Folgen Sie den Anweisungen im Browser.

### Funktionszertifikat

Geben Sie alle E-Mail-Adressen ein, die in das Zertifikat geschrieben werden sollen. Beachten Sie die Hinweistexte im Portal!

![](img/ca-portal_de_request_10_functional_add_emails.webp){ width="800" }

Jetzt werden nochmal alle Daten angezeigt, die ins Zertifikat geschrieben werden. Wählen Sie `Absenden`, wenn alles
korrekt ist:

![](img/ca-portal_de_request_20_functional_submit.webp){ width="800" }

<a name="backup-p12"></a>
## Backup erstellen

Machen Sie spätestens **jetzt** eine Sicherungskopie. Sie brauchen bis zum Ende ihres (Dienst-)Verhältnisses mit dem KIT
**jedes** Schlüssel/Zertifikats-Paar (in der Regel ist das die `.p12`-Datei), für das Sie jemals verschlüsselte E-Mails
empfangen haben.

Sichern Sie sowohl die Zertifikatsdatei als auch das Passwort so, dass sie beides in vielen Jahren noch sicher
wiederfinden und lesen können. Aus Sicherheitsgründen empfielt es sich, beides getrennt voneinander zu sichern.

??? note ":construction: Work in Progress"
    Dieser Abschnitt ist leider noch etwas rudimentär & unvollständig.

<a name="install-certificate"></a>
## Neues Zertifikat einbinden

Die eben heruntergeladene Datei kann normalerweise durch Doppelklick (Windows, macOS) in das Betriebssystem importiert werden.

Hinweis für Windows-Nutzer: Setzen Sie beim beim Import die Option _Schlüssel als exportierbar markieren_. Dann können
Sie – beispielsweise beim Wechsel ihres Rechners – Zertifikat und privaten Schlüssel von diesem Rechner auf das neue
Gerät kopieren:

![](img/windows_11_certificate_import_allow-export_de.webp){ width="531" }

Thunderbird unter Linux muss das Zertifikat direkt in der Anwendung importieren: _Einstellungen_ → _Datenschutz & Sicherheit_ → _Zertifikate verwalten…_.
Dort den Reiter _Ihre Zertifikate_ wählen, dann _Import…_.  Dann in den Einstellungen des passenden E-Mail-Kontos das Zertifikat für Verschlüsselung und Signatur auswählen.
<!-- TODO: Ausführliche Anleitung mit Bildern machen -->

## E-Mail-Client einrichten

* Anleitung für [Outlook in Windows](/guides/de/configure_outlook/)
* Anleitung für [macOS & Apple Mail](/guides/de/install_p12_macos/)
* Anleitung für [Thunderbird (externer Link zur Uni Heidelberg)](https://www.urz.uni-heidelberg.de/de/support/anleitungen/import-der-smime-zertifikate-in-thunderbird){:target="_blank"}
