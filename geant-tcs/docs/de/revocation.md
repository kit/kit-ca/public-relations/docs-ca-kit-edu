# Zertifikate sperren

Gelegentlich ist es nötig, Zertifikate bereits vor ihrem regulären Ablaufdatum aus dem Verkehr zu ziehen.
Da ein aktiv genutztes Zertifikat potenziell auf unzähligen Rechnern als Kopie vorliegt, kann man Zertifikate
nicht einfach _löschen_. Stattdessen werden Zertifikate _zurückgezogen_ bzw. _gesperrt_ (englisch: _revoked_).
Dabei werden diese Zertifikate auf zentralen [Sperrlisten](https://de.wikipedia.org/wiki/Zertifikatsperrliste){:target="_blank"}
beziehungsweise [Sperr-Servern](https://de.wikipedia.org/wiki/Online_Certificate_Status_Protocol){:target="_blank"} des
CA-Anbieters veröffentlicht. Client wie Outlook und Thunderbird prüfen damit regelmäßig, ob Zertifikate
noch verwendet werden sollen.

!!! warning
    Eine solche Sperre kann nicht widerrufen werden! Es gilt also, entsprechend Vorsicht walten zu lassen beim Sperren.

Der Sperr-Status eines Zertifikats hat keinen Einfluss auf seine kryptografische Nutzung. Solange man
im Besitz des passenden privaten Schlüssels ist, kann man auch in der Zukunft verschlüsselte E-Mails für
gesperrte Zertifikate entschlüsseln. Deswegen gilt auch hier: niemals Zertifikate löschen, für die man noch
verschlüsselte E-Mails hat oder erwartet.

## Zertifikate sperren am KIT

Es gibt leider derzeit keinen Self-Service zum Sperren von Zertifikaten.

Der aktuelle Prozess sieht so aus, dass eine _berechtigte Person_ eine signierte E-Mail an
[ca@kit.edu](mailto:ca@kit.edu?subject=Bitte%20Zertifikat%28e%29%20sperren%0A&body=Hallo%21%0A%0ABitte%20sperren%20Sie%20das%20Zertifikat%20mit%20den%20folgenden%20Daten%0A%0A%2A%20E-Mail%3A%20CHANGEME%40kit.edu%0A%2A%20Seriennummer%3A%200x1234567890abcdef00000001%0A%2A%20Grund%3A%20%E2%80%A6%0A%0A)
schickt mit Subject `Bitte Zertifikat(e) sperren` und im Mailtext eine Liste aller zu sperrenden Zertifikate.
Bitte pro Zertifikat eine der zugehörigen E-Mail-Adressen sowie die Seriennummer angeben. Diese finden Sie
entweder im Zertifikatsspeicher ihres Betriebssystems (`certmgr.msc` unter Windows und
_Schlüsselbundverwaltung_ in macOS) oder auf der [Zertifikatssuche der KIT-CA](https://search.ca.kit.edu/){:target="_blank"}.

_Berechtigte Person_ sind entweder die ursprüngliche beantragende Person oder ein passender IT-Beauftragter.
Bei Funktionszertifikaten außerdem alle Personen, die Zugriff auf das betroffene Postfach haben.