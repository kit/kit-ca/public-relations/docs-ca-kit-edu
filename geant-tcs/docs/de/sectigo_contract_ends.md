# Ende des Vertrages von GÉANT mit Sectigo

Wie auf der [Hauptseite](index.md) beschrieben, bezieht die KIT-CA ihre Zertifikate über GÉANT TCS, wobei der Betrieb der Zertifizierungsstelle an Sectigo vergeben haben.
Dieser Vertrag sollte eigentlich bis April 2025 laufen, würde jetzt aber einseitig durch Sectigo gekündigt, wobei Sectigo der Meinung ist, dass ein Vertragsende bereits zum 10.01.2025 vertraglich möglich ist.
Auch wenn GÉANT und die DFN-PKI das anders sehen, reicht für einen Rechtstreit die Zeit nicht aus.

Sowohl GÉANT als auch die DFN-PKI bemühen sich, zeitnah einen neuen CA-Betreiber zu finden, damit eine Versorgungslücke so klein wie möglich gehalten wird.
Durch die Kurzfristigkeit der Aufkündigung wird sich eine Lücke wahrscheinlich nicht vermeiden lassen.

**Die KIT-CA kann also ab 10.01.2025 keine Zertifikate via Sectigo mehr ausstellen.**

!!! warning "Übergangsvertrag abgeschlossen"
    Am 04.12.2024 hat die DFN-PKI mitgeteilt, dass ein Übergangsvertrag mit der Certificate Authority HARICA unterzeichnet wurde.
    Über diesen können wir dann hoffentlich nach dem Sectigo-Vertragsende weiterhin Zertifikate ausstellen.
    Der genaue Prozess ist noch unbekannt, wird auf unserer Seite allerdings definitiv einen höheren, manuellen Aufwand verursachen.
    Unterstützen Sie uns bitte weiterhin, indem Sie ablaufende Zertifikate bereits jetzt erneuern.

!!! quote "Ich möchte *alles* wissen!"
    Alles, was wir aktuell wissen, dokumentiert die DFN-PKI in einem [Blogartikel](https://doku.tid.dfn.de/de:dfnpki:tcsfaq:aktuellesituation).
    Mehr Informationen als die DFN-PKI haben wir nicht.
    Bitte sehen Sie von Anfragen ab, die nicht die hier erklärten Maßnahmen der KIT-CA betreffen.

### Wie sieht die Strategie der KIT-CA aus, um die Situation zu entschärfen? {data-toc-label="Strategie der KIT-CA"}

Um möglichst umfassend zu informieren und die Anzahl der betroffenen Nutzenden möglichst kleinzuhalten, planen wir mehrstufige Informationen.

- Information der IT-Beauftragten über die Situation durch Verweis auf diese Seite.
- 27.11.2024: Vortrag auf der ITB-Versammlung.
- Informationsmail an Kontakte von Serverzertifikaten und alle KIT-Angehörigen, die ein Mailzertifikat haben, das innerhalb eines Jahres abläuft.
- 04.12.2024: Angebot eines Informations- und Diskussionstermins via Teams, um Fragen und Bedenken zu klären. Dieser werden für IT-Beauftragte ausgerichtet, wenn Sie als Endanwender Fragen haben, bitte geben Sie diese an die IT-Beauftragten Ihrer Organisation weiter.

### Was passiert mit meinen Zertifikaten nach Vertragsende? {data-toc-label="Zertifikate nach Vertragsende"}

Ausgestellte, gültige Zertifikate behalten nach unserem besten Wissen und Gewissen Ihre Gültigkeit.
Entsprechend der Zertifizierungsrichtlinie und den Vertragsbedingungen ist eine Sperrung nach Vertragsende nicht erlaubt.
Sectigo würde sich damit bei vielen globalen Vertrauensankern (z.B. Betriebssysteme und Browser) unbeliebt machen, weswegen wir zuversichtlich sind, dass Sectigo keine gültigen Zertifikate sperren wird.

### Mein Mailzertifikat läuft bald ab, was soll ich tun? {data-toc-label="Ablaufende Mailzertifikate"}

Vermutlich lesen Sie diesen Abschnitt, weil Sie von uns eine Mail mit der Information bekommen haben, dass Sie Ihr Zertifikat erneuern sollten.
Bitte folgen Sie unserer [Anleitung](https://ca.kit.edu/p/nutzer), um ein neues Zertifikat zu erhalten.

Nach Ausstellung wird in den meisten Fällen für an Sie verschlüsselt gesendete Mails sofort das neue Zertifikat verwendet, achten Sie daher unbedingt auf eine korrekte Einrichtung.
Es funktionieren bis zum Ablauf des alten Zertifikats beide Varianten weiter (beachten Sie dazu auch den Abschnitt "[Was passiert mit meinen Zertifikaten nach Vertragsende?](sectigo_contract_ends.md#was-passiert-mit-meinen-zertifikaten-nach-vertragsende)").

Falls in Ihrer Mail mehrere Zertifikate erwähnt sind, reicht es **ein** neues Zertifikat zu beantragen.

Mehr Details über die zu beachtenden Konzepte hinter Zertifikaten gibt es unter [Konzepte](concepts.md).

### Bei uns fängt in Q1/Q2 2025 eine neue Person an, die für deren Arbeit zwingend ein Mailzertifikat braucht! {data-toc-label="Mailzertifikat für neue Mitarbeitende in Q1/Q2"}

Bei ausreichender Begründung können wir auch für noch nicht existierende Accounts ein Funktionszertifikat ausstellen, da die für den Account vergebenen Mailadressen vorhersagbar sind.
Der Prozess ist allerdings aufwändiger für uns.
Bitte schreiben Sie uns eine Mail an [ca@kit.edu](mailto:ca@kit.edu) mit einer ausreichenden Begründung, warum ein Zertifikat zwingend nötig ist.

Falls Sie diesen Abschnitt erst nach dem 10.01.2025 lesen, schreiben Sie bitte eine Mail an [ca@kit.edu](mailto:ca@kit.edu).

### Mein Serverzertifikat läuft in Q1/Q2 2025 ab, was soll ich tun? {data-toc-label="Ablaufende Serverzertifikate"}

Wir werden rechtzeitig zentral alle aktuell über Sectigo ausgestellten Serverzertifikate erneuern.
Dabei wird (entgegen unserer üblichen Empfehlungen) der private Schlüssel des aktuellen Zertifikates weiterverwendet, Sie müssen dann nur das Zertifikat tauschen.
Das neue Zertifikat werden Sie automatisch per Mail an die beim Erstantrag angegebene Mailadresse erhalten.

### Ich muss in Q1/Q2 2025 einen wichtigen Dienst aufsetzen und benötige Serverzertifikate! {data-toc-label="Serverzertifikat für neuen Dienst in Q1/Q2"}

So gut wie alle Anwendungsfälle für Serverzertifikate lassen sich mit Zertifikaten von Let's Encrypt abbilden.
Dies gilt auch für Mutual Authentication und Systeme, die nicht selbst aus dem Internet erreichbar sind.
Eine Anleitung zur Einrichtung finden Sie in unserer [ACME4NETVS-Dokumentation](https://docs.ca.kit.edu/acme4netvs).

Falls eine Umsetzung mit Let's Encrypt für Sie so kurzfristig nicht machbar ist, haben wir unseren [Transition-Dienst](https://transition.ca.kit.edu/) wiederbelebt.
Dieser ermöglicht es, Zertifikate von Let's Encrypt zu bekommen, ohne einen ACME-Client einzurichten.
Das Zertifikat ist dann, wie bei Let's Encrypt üblich, nur 3 Monate gültig.

Sollten Sie dennoch einen Anwendungsfall haben, der unbedingt organisationsvalidierte Zertifikate benötigt, folgen Sie **noch vor der Einstellung des Betriebes am KIT über Weihnachten und Silvester** unserer [Anleitung zur Beantragung von OV-Zertifikaten](server.md) mit den voraussichtlich benötigten Domainnamen.

Falls Sie diesen Abschnitt erst nach dem 10.01.2025 lesen und für Sie keine der anderen Lösungen funktioniert, schreiben Sie bitte eine Mail an [ca@kit.edu](mailto:ca@kit.edu).
