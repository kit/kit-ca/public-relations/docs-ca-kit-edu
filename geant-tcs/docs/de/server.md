# Serverzertifikate mit GÉANT TCS

Wir empfehlen, die Ausstellung von Serverzertifikaten zu automatisieren und Zertifikate von [Let's Encrypt mit acme4netvs](https://docs.ca.kit.edu/acme4netvs/en/) zu verwenden.
Die Dokumentation ist aktuell nur auf Englisch verfügbar.

Falls das für Sie keine Option ist (z.B. wenn der Server keine Verbindung ins Internet aufbauen darf), können auch mit GÉANT TCS Serverzertifikate beantragt werden.

### Verfügbare Zertifikatsprofile

Über TCS stehen folgende Zertifikatsprofile zur Verfügung:

| Profil              | Zertifikatslaufzeit | Verfügbare Schlüsselarten                                                                       |
|---------------------|---------------------|-------------------------------------------------------------------------------------------------|
| OV Multi-Domain     | 365 Tage            | Elliptic Curve (EC) P-256 (256 Bit), P-384 (384 Bit), RSA 2048 Bit, 3072 Bit, 4096 Bit          |
| IGTF Multi-Domain   | 395 Tage            | Elliptic Curve (EC) P-256 (256 Bit), P-384 (384 Bit) RSA 2048 Bit, 3072 Bit, 4096 Bit, 8192 Bit |

!!! warning "Warnung"
    IGTF Multi-Domain ist speziell für das Grid-Umfeld konzipiert und diesem für die Verwendung vorbehalten. Die Berechtigung dafür wird vor Ausstellung durch uns geprüft.

### Antragsprozess

Aktuell ist die Beantragung von Serverzertifikaten noch nicht im [KIT-CA Portal](https://portal.ca.kit.edu){:target="_blank"} implementiert.
Solange kann folgender Übergangsprozess verwendet werden:

1. [Erstellen Sie einen Certificate Signing Request (CSR)](csr_generation.md) mit allen im Zertifikat benötigten Domains als Subject Alternative Name (SAN). Im Common Name (CN) kann eine beliebige Domain stehen. Achten Sie darauf, dass der Private Key einem der oben aufgeführten Schlüsselarten entspricht, ansonsten kann das Zertifikat nicht ausgestellt werden. 
2. Senden Sie den CSR mit per Mail an [ca@kit.edu](mailto:ca@kit.edu?subject=%5BTCS%20Certificate%20Request%5D%20Serverzertifikatsantrag&body=Hallo%2C%0A%0Abitte%20stellen%20Sie%20ein%20Serverzertifikat%20f%C3%BCr%20den%20angeh%C3%A4ngten%20CSR%20aus.%0A%0A%2A%20Diese%20Mail%20ist%20signiert%0A%2A%20Ich%20bin%20in%20folgender%20Position%20berechtigt%2C%20Zertifikate%20f%C3%BCr%20den%20Server%20zu%20erhalten%3A%20Begr%C3%BCndung%0A%2A%20Das%20Zertifikat%20und%20Benachrichtigungen%20gehen%20an%3A%20Mailadresse%0A%0ALet%27s%20Encrypt%20kann%20leider%20nicht%20verwendet%20werden%3A%0ABegr%C3%BCndung%0A%0AViele%20Gr%C3%BC%C3%9Fe%0AName). Die Mail muss folgende Bedingungen erfüllen. 
    * Die Mail muss von einer für die Domain berechtigten Person S/MIME-signiert sein
    * Der Betreff muss mit `[TCS Certificate Request]` beginnen
    * Die Mailadresse, die das fertige Zertifikat und Benachrichtigungen dazu erhalten soll, muss explizit im Mailtext angegeben sein
    * Optional: Wir freuen uns über eine kurze Erklärung, warum Let's Encrypt für Sie (aktuell) keine Option ist
3. Wir stellen das Zertifikat nach Prüfung der Berechtigungen aus.
4. Sie erhalten einen Downloadlink zum Zertifikat und der Zertifikatskette per Mail von `support@cert-manager.com`.

!!! note "Notiz"
    Wenn das System zuvor ein Zertifikat der DFN-CA Global verwendet hat und die betriebene Software nicht auf den Zertifikatsspeicher des Betriebssystems zurückgreift, muss die Zertifikatskette importiert werden. Diese ist in der Zertifikatsmail verlinkt. Das ist z.B. oft bei Java-Software der Fall.
