Bitte geben Sie den Hostnamen an, für den Sie ein Zertifikat beantragen müssen:  
<input data-input-for="CSR_Server_CN">

!!! important "Wichtig"
    Niemand bei der KIT-CA benutzt diesen Weg, wir haben also nur sehr begrenzt Erfahrung damit.
    Bei Problemen hilft potenziell ein Blick in die offizielle Dokumentation ([certreq.exe](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/certreq_1), [certutil.exe](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/certutil){:target="_blank"}).

Antragsdateien können unter Windows auch auf der Kommandozeile erstellt werden.
Hierfür muss zunächst eine Datei **xCSR_Server_Templatefilex** mit folgendem Inhalt erstellt werden:

```
[NewRequest]
Exportable = TRUE
KeyLength = 4096
HashAlgorithm = sha256
MachineKeySet = TRUE
Subject = "CN=xCSR_Server_CNx"
RequestType = PKCS10
UserProtected = FALSE
```

Um [Subject Alternative Names (SANs)](http://en.wikipedia.org/wiki/SubjectAltName){:target="_blank"} zu setzen, fügen Sie eine neue Sektion `[Extensions]` in die zuvor erstellte Datei `xCSR_Server_Templatefilex` mit den gewünschten Hostnamen (zwischen `_continue_ = "DNS=` und `&"`) ein:

```
[Extensions]
2.5.29.17 = "{text}"
_continue_ = "DNS=<b>weiterer-hostname.ifmb.kit.edu</b>&"
_continue_ = "DNS=<b>noch-ein-hostname.ifmb.kit.edu</b>&"
```

Die eigentliche Antragsdatei kann danach mit dem folgenden Aufruf erstellt werden.
Hierbei wird die Datei `xCSR_Server_Reqfilex` mit der neu generierten Antragsdatei überschrieben.

```
certreq -new xCSR_Server_Templatefilex xCSR_Server_Reqfilex
```

Details Ihres Zertifikatsantrages können Sie mit folgendem Befehl ansehen:

```
certutil -dump xCSR_Server_Reqfilex
```
