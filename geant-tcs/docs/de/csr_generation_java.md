Bitte geben Sie den Hostnamen an, für den Sie ein Zertifikat beantragen müssen:  
<input data-input-for="CSR_Server_CN">

Zunächst muss ein Schlüsselpaar aus privatem und öffentlichem Schlüssel generiert werden.
Dies kann mit folgendem Befehl über die Kommandozeile erfolgen:

```
keytool -genkey \
 -alias xCSR_Server_CNx \
 -dname "CN=xCSR_Server_CNx" \
 -keyalg RSA \
 -keysize 4096 \
 -keystore xCSR_Server_Keystorex
``` 

Aus diesem Schlüsselpaar kann jetzt der Zertifikatsantrag generiert werden:

```
keytool -certreq \
 -alias xCSR_Server_CNx \
 -file xCSR_Server_Reqfilex \
 -keystore xCSR_Server_Keystorex
```