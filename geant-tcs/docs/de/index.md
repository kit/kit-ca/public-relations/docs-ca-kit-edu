# GÉANT Trusted Certificate Services

Bis zum 31.08.2023 nutzte die KIT-CA die DFN-CA Global der DFN-PKI für die Ausstellung von Zertifikaten für unsere Nutzenden.
Da die DFN-PKI nicht ausreichend personelle Kapazität hat, um die regelmäßig erforderlichen Änderungen für global vertraute Zertifizierungsstellen aufrechtzuerhalten, wurde die DFN-CA Global eingestellt.

Der Nachfolger ist GÉANT Trusted Certificate Services (TCS).
GÉANT ist ein Zusammenschluss der europäischen nationalen Forschungs- und Bildungsnetze (NRENs) und bietet verschiedene Dienste und Infrastrukturen für diese nationalen Organisationen an.
Einer dieser Dienste ist TCS.
Über das Deutsche Forschungsnetz (DFN), das NREN in Deutschland, erhält das KIT-CA Zugang zu TCS und kann Zertifikate für Benutzer und Server erhalten.

TCS wird von einem Certification Authority Operator, derzeit Sectigo, betrieben.

!!! warning "GÉANT TCS: Vertragsende mit Sectigo"
    Der Vertrag von GÉANT TCS mit Sectigo zum Betrieb der Zertifizierungsstelle endet am 10.01.2025.
    Die KIT-CA wird vorübergehend keine neuen Zertifikate ausstellen können.
    Alle Informationen dazu finden Sie in unserem [Artikel](/geant-tcs/de/sectigo_contract_ends).

Der Betreiber der Zertifizierungsstelle wird alle 5 Jahre neu ausgeschrieben, sodass es regelmäßig zu Änderungen kommen kann.
Das bedeutet, dass sich auch der Zertifikatsbeantragungsprozess alle 5 Jahre ändern kann, was jedes Mal zu Verwirrung auf der Benutzerseite führt.
Als KIT-CA ist es uns wichtig, dass die Beantragung von Zertifikaten am KIT für alle Nutzer transparent ist und dass der Prozess so weit wie möglich gleich bleibt.
Daher bauen wir derzeit ein eigenes Portal für die Beantragung von Zertifikaten auf, das auch bei einem Wechsel des CA-Betreibers gleich bleibt.

Die erste Version des CA-Portals, die noch zum Teil den Prozess von Sectigo verwendet, ist ab sofort unter [https://portal.ca.kit.edu](https://portal.ca.kit.edu){:target="_blank"} verfügbar.
Eine Dokumentation für den gesamten Prozess gibt es hier:

* [E-Mail-Zertifikate](user.md)
* [Serverzertifikate](server.md)
