Bitte geben Sie den Hostnamen an, für den Sie ein Zertifikat beantragen müssen:  
<input data-input-for="CSR_Server_CN">

Mit [OpenSSL](https://www.openssl.org/){:target="_blank"} können Schlüssel und Antragsdatei über die Kommandozeile generiert werden.
`FQHN` ist der [vollqualifizierte Hostname](http://de.wikipedia.org/wiki/Fully-Qualified_Host_Name){:target="_blank"}.

```
openssl req -newkey rsa:4096 \
 -out xCSR_Server_Reqfilex \
 -keyout xCSR_Server_Keyfilex \
 -nodes \
 -subj '/CN=xCSR_Server_CNx'
```

Der Parameter `-nodes` sorgt dafür, dass der geheime Schlüssel unverschlüsselt gespeichert wird.
Das ist bei den meisten Serveranwendungen sinnvoll, da beim Start des Dienstes kein Passwort eingegeben werden muss.
Um den geheimen Schlüssel mit einem Passwort zu verschlüsseln, lassen Sie einfach `-nodes` weg.

Um [Subject Alternative Names (SANs)](http://en.wikipedia.org/wiki/SubjectAltName){:target="_blank"} zu setzen, kann folgender Befehl verwendet werden.
Dabei nicht vergessen, die tatsächlichen Hostnamen zu ersetzen.
Jedem Hostname muss `DNS:` vorangestellt werden, mehrere Einträge sind mit `,` zu separieren.

```
openssl req -newkey rsa:4096 \
 -out xCSR_Server_Reqfilex \
 -keyout xCSR_Server_Keyfilex \
 -nodes \
 -subj '/CN=xCSR_Server_CNx' \
 -addext 'subjectAltName = DNS:weiterer-hostname.ifmb.kit.edu,DNS:noch-ein-hostname.ifmb.kit.edu'
```
