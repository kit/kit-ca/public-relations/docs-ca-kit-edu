# Zertifikate (eindeutig) identifizieren

Einige Prozesse im Umgang mit Zertifikaten setzen voraus, dass die beteiligten Personen und Systeme einzelne Zertifikate
eindeutig benennen können. Diese Anleitung versucht, die hierfür nötigen Grundlagen zu vermitteln.

## Was sind Zertifikate?

Stark vereinfacht besteht ein [X.509-Zertifikat](https://de.wikipedia.org/wiki/X.509#Zertifikate) aus einem öffentlichen kryptographischen Schlüssel, einem Satz
an Attributen sowie einer Signatur durch eine Zertifizierungsstelle.

Die Attribute beschreiben das Subjekt (bei Personenzertifikaten: den/die Besitzer:in) und definieren erlaubten Einsatzzwecke
und Gültigkeits-Zeitraum. Die Signatur durch die Zertifizierungsstelle verhindert Veränderungen und versichert die
Korrektheit der gemachten Angaben.

- Seriennummer (*Serial Number*)
- Subject (*common name*)
- Fingerprint (MS5/SHA1/SHA256)
- Friendly Name