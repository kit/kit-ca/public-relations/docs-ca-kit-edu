!!! warning "Diese Anleitung ist veraltet!"
    Sectigo hat am 14.9.2024 diesen Teil ihres Prozesses geändert. Seitdem ist die Einstellung “Secure AES256-SHA256”
    für fast alle Systeme die korrekte Option.
    Diese Anleitung wird nur noch für Nutzer alter Zertifikate hier bereitgestellt. 

# Reparieren falsch gespeicherter Zertifikatsdateien

Die von Sectigo vorgegebene Einstellung beim Speichern der Zertifikatsdatei ist leider auf den gängigen Systemen (Windows und alles von Apple) nicht
benutzbar:

![](../img/tcs_sectigo_personal_certificate_request_certificate_enrollment_form_secure_cropped.webp){ width="404" }

Diese Einstellung funktioniert überall:

![](../img/tcs_sectigo_personal_certificate_request_certificate_enrollment_form_compatible_cropped.webp){ width="404" }

Diese Anleitung erklärt, wie man mit Bordmitteln oder dem (frei verfügbaren und oft schon installiertem) [Firefox-Browser](https://www.mozilla.org/de/firefox/){:target="_blank"}
die Zertifikatsdatei in ein brauchbares Format umwandelt.

## Symptome

Möglicherweise Sie sich nicht sicher, ob Sie von diesem konkreten Problem betroffen sind. Im Folgenden sind ein paar
Diagnose-Möglichkeiten beschrieben.

=== ":material-microsoft-windows: Windows"

    #### Frage nach Smartcard

    Beim Import einer falsch gespeicherten Zertifikatsdatei fragt Windows nach einer (nicht vorhandenen) Smartcard:
    
    ![](../img/tcs_sectigo_fix_p12_crypto_en_001_w11_cert_import_smartcard_dialog.webp){ width="400" }

    Danach kommt am Ende diese (nicht korrekten) Erfolgsmeldung:

    ![](../img/tcs_sectigo_fix_p12_crypto_de_002_windows_import_successful.webp){ width="227" }

    In diesem Fall sind Sie betroffen.
    
    #### Falscher Ordner im Zertifikatsspeicher

    Das Zertifikate landet im Windows-Zertifikatsspeicher unter *Andere Personen :material-chevron-right: Zertifikate*
    statt unter *Eigene Zertifikate :material-chevron-right: Zertifikate*.

    In diesem Fall sind Sie betroffen.

    #### Privater Schlüssel nicht exportierbar

    Um zu prüfen ob die importiere Zertifikatsdatei tatsächlich nicht korrekt gelesen wurde kann man versuchen diese
    zu exportieren. öffnen Sie den Windows-Zertifikatsspeicher: drücken Sie die Tasten ++windows+r++, geben Sie
    `certmgr.msc` ein und klicken Sie ++ok++. Wählen Sie das defekte Zertifikat aus, öffnen sie das Kontextmenü und
    wählen Sie *Alle Aufgaben*:material-chevron-right:*Exportieren…*.

    ![](../img/tcs_sectigo_fix_p12_crypto_de_006_diag_certmgr_export_01.webp){ width="665" }

    Wenn Sie auf der zweiten Seite des Zertifikatexport-Assistenten **nicht** die Möglichkeit haben den privaten Schlüssel
    zu exportieren, ist Ihre Zertifikatsdatei defekt:

    ![](../img/tcs_sectigo_fix_p12_crypto_de_006_diag_certmgr_export_03.webp){ width="534" }

    #### Verschlüsselungstest schlägt fehl

    Öffnen Sie eine Shell (++windows+r++ und dann `cmd` oder `powershell` starten). Geben Sie `certutil.exe -user -store My`
    ein. Jetzt werden sämtliche Zertifikate aufgelistet und für alle Zertifikate mit privatem Schlüssel wird ein
    Verschlüsselungstest durchgeführt.

    Finden Sie in der Ausgabe das relevante Zertifikat mittels `Nicht vor:` und `Antragsteller:`. Falls der Datensatz
    mit `Gespeicherter Schlüsselsatz fehlt` endet sind sie betroffen. Ein `Verschlüsselungstest wurde durchgeführt` zeigt
    an dass der private Schlüssel vorrhanden ist und benutzt werden kann.

    #### Signieren und Entschlüsseln in Outlook schlägt fehlt 
    
    Outlook verweigert das Senden von signierten E-Mails mit dieser generischen Fehlermeldung:
    
    ![](../img/tcs_sectigo_fix_p12_crypto_de_001_outlook_error.webp){ width="820" }

    Dies kann allerdings auch andere Gründe haben.

=== ":material-apple: macOS"

    Nach Doppelklick auf die Zertifikatsdatei von Sectigo liefert macOS nach dreimaliger Eingabe des korrekten
    Passwords diese beiden Fehlermeldungen.
    
    ![](../img/tcs_sectigo_fix_p12_crypto_en_001_macOS_error.webp){ width="542" }

    Der hier dokumentierte Weg mit Firefox funktioniert **nicht vollständig** auf macOS. Es ist möglich, die Zertifikatsdatei
    zu konvertieren. Der Import in den macOS-Schlüsselbund muss aber über eine Shell im Terminal durchgeführt werden. Dies ist
    [am Ende](#import_in_macos) dieser Anleitung beschrieben
    
    Alternativ können Sie auch ein neues Zertifikat zu beantragen und beim Speichern das korrekte Format auswählen.

=== ":fontawesome-brands-linux: OpenSSL"
    Stellen Sie sicher, dass die OpenSSL Version 3[^2] und nicht die macOS-native Version[^3] benutzen.
    [^2]: `openssl version` sollte `OpenSSL 3.x.y …` liefern
    [^3]: `openssl version` sollte `… Library: OpenSSL` und nicht `… Library: LibreSSL` liefern

    Führen Sie diesen Befehl aus (ersetzen Sie `smime_suspekt.p12` mit dem Pfad und Dateinamen ihrer Zertifikatsdatei):

    ```shell
    openssl pkcs12 -info -noout -in smime_suspekt.p12
    ```

    Die Ausgabe einer falsch (*Secure AES256-SHA256*) gespeicherten Datei sieht etwa so aus:
    
    ```shell hl_lines="1 3 9"
    MAC: sha256, Iteration 20000
    MAC length: 32, salt length: 64
    PKCS7 Encrypted data: PBES2, PBKDF2, AES-256-CBC, Iteration 20000, PRF hmacWithSHA256
    Certificate bag
    Certificate bag
    Certificate bag
    Certificate bag
    PKCS7 Data
    Shrouded Keybag: PBES2, PBKDF2, AES-256-CBC, Iteration 20000, PRF hmacWithSHA256
    ```
    
    Korrekt (*Compatible TripleDES-SHA1*) gespeichert kommt etwa so etwas raus: 
    
    ```shell hl_lines="1 3 9"
    MAC: sha1, Iteration 2048
    MAC length: 20, salt length: 64
    PKCS7 Encrypted data: pbeWithSHA1And3-KeyTripleDES-CBC, Iteration 2048
    Certificate bag
    Certificate bag
    Certificate bag
    Certificate bag
    PKCS7 Data
    Shrouded Keybag: pbeWithSHA1And3-KeyTripleDES-CBC, Iteration 2048
    ```

## Zertifikatsdatei reparieren

### Defektes Zertifikat löschen

Falls die Zertifikatsdatei bereits installiert wurde: öffnen Sie den Windows-Zertifikatsspeicher. Drücken Sie die
Tasten ++windows+r++, geben Sie `certmgr.msc` ein und klicken Sie ++ok++:

![](../img/tcs_sectigo_fix_p12_crypto_de_005_run_certmgr.webp){ width="400" }

Suchen Sie das defekte Zertifikat. Dies befindet sich normalerweise unter *Andere Personen:material-chevron-right:Zertifikate*.
Löschen Sie das Zertifikat über das Kontext-Menü (Rechtsklick):

![](../img/tcs_sectigo_fix_p12_crypto_de_010_certmgr.msc_delete_broken.webp)

!!! tip "Bitte alle defekten Zertifikate löschen"
    Stellen Sie bitte sicher, dass es keine weiteren solche Zertifikate in ihrem Windows-Zertifikatsspeicher gibt.

### :material-firefox: Firefox installieren

Installieren Sie – sofern nicht schon vorhanden – den Firefox-Browser[^1].

[^1]: Ist bei verwaltete KIT-Systeme vorinstalliert. Ansonsten:
[:simple-mozilla: Mozilla](https://www.mozilla.org/de/firefox/download/thanks/ "Direkt bei Mozilla herunterladen"){:target="_blank"},
[winget](https://winget.run/pkg/Mozilla/Firefox "Mit winget, dem Windows-Paketmanager, installieren"){:target="_blank"},
[Chocolatey](https://community.chocolatey.org/packages/Firefox "Mit chocolaty, einem Paketmanager, installieren"){:target="_blank"},
[scoop](https://scoop.sh/#/apps?q=extras%2Ffirefox "Mit scoop, einem Paketmanager, installieren"){:target="_blank"}

### Zertifikatsdatei konvertieren

Starten Sie Firefox. Öffnen Sie das Menü (:material-menu:) und wählen Sie *Einstellungen*:

![](../img/tcs_sectigo_fix_p12_crypto_de_020_firefox_open_settings.webp){ width="300" }

Wählen Sie links den Abschnitt *Datenschutz & Sicherheit*. Suchen Sie im rechten Teil den Abschnitt **Zertifikate** und
öffnen Sie die Firefox-Zertifikatsverwaltung mittels *Zertifikate anzeigen…*:

![](../img/tcs_sectigo_fix_p12_crypto_de_021_firefox_settings_open_certmgr.webp){ width="890" }

Wählen Sie den Reiter *Ihre Zertifikate*, dann *Importieren*:

![](../img/tcs_sectigo_fix_p12_crypto_de_022_firefox_certmgr_import_p12.webp){ width="892" }

Wählen Sie die Zertifikatsdatei, die Sie beim Beantragen von Sectigo (heißt `smime_*.p12`, wenn Sie die nicht
umbenannt haben) heruntergeladen haben.

Geben Sie das Passwort ein, welches Sie beim Exportieren auf der Sectigo-Webseite verwendet haben:

![](../img/tcs_sectigo_fix_p12_crypto_de_023_firefox_import_password_dialog.webp){ width="650" }

<a name="firefox_and_os_keychain"></a>
!!! warning "Nur die Firefox-eigenen Zertifikate wählen"
    Firefox hat seit [einiger Zeit](https://blog.mozilla.org/security/2021/07/28/making-client-certificates-available-by-default-in-firefox-90/){:target="_blank"}
    eine direkte Anbindung an den Zertifikatsspeicher von Windows bzw. die Keychain von macOS. Das ist hier leider
    kontraproduktiv. Man erkennt solche “firefox-fremde” Zertifikate an der Spalte *Kryptographie-Modul*/*Security Device*.
    Einträge mit *Software-Sicherheitsmodul*/*Software Security Device* befinden sich nativ in Firefox. Einträge mit
    *OS Client Cert Token* kommen aus dem Betriebssystem. Stellen Sie sicher hier nur auf Einträgen der ersten Art zu arbeiten.
    
    ![](../img/tcs_sectigo_fix_p12_crypto_de_090_firefox_certmgr_module_dark.webp#only-dark){ width="893" }
    ![](../img/tcs_sectigo_fix_p12_crypto_de_090_firefox_certmgr_module_light.webp#only-light){ width="893" }

Wählen Sie das gerade importierte Zertifikat aus und wählen Sie *Sichern…*:

![](../img/tcs_sectigo_fix_p12_crypto_de_024_firefox_certmgr_export.webp){ width="893" }

Setzen Sie ein sicheres Export-Passwort. Sie können das vorher verwendete Passwort benutzen:

![](../img/tcs_sectigo_fix_p12_crypto_de_025_firefox_export_password.webp){ width="997" }

Sichern Sie das konvertierte Zertifikat unter einem neuen Dateinamen.

## Reparierte Datei im Betriebssystem importieren 

=== "Import in Windows :material-microsoft-windows:"

    Öffnen Sie das konvertierte Zertifikat durch Doppelklick auf die neue Datei. Durchlaufen Sie den Import-Assistenten.
    
    Das reparierte Zertifikat sollte jetzt im Windows-Zertifikatsspeicher unter
    *Eigene Zertifikate:material-chevron-right:Zertifikate* auftauchen:
    
    ![](../img/tcs_sectigo_fix_p12_crypto_de_030_certmgr.msc_see_fixed.webp)

    <a name="import_in_macos"></a>
=== "Import in macOS :material-apple:"

    [Öffnen Sie das Terminal](https://support.apple.com/de-de/guide/terminal/apd5265185d-f365-44cb-8b09-71a064a42125/mac){ :target="_blank" }.
    Navigieren Sie in das Verzeichnis mit der konvertierten Zertifikatsdatei (potenziell einfach `cd ~/Downloads/`).
    Importieren Sie die konvertierte Zertifikatsdatei mit diesem Befehl (ersetzen Sie `new.p12` durch den von ihnen beim
    Speichern in Firefox gewählten Dateinamen):
    
    ```shell
    security import new.p12
    ```
    
    Geben Sie das Passwort ein, welches Sie beim Speichern in Firefox gewählt haben.

## Nachgang

Wenn Sie ihr repariertes Zertifikat erfolgreich genutzt haben, löschen Sie die defekte Datei oder markieren die diese
auffällig (beispielsweise mit einem Dateinamen, der mit `SECTIGO_DEFEKT_` anfängt).