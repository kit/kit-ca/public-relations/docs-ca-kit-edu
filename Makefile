.PHONY: acme4netvs acme4netvs_clean acme4netvs_upload main main_clean main_upload index_upload geant-tcs geant-tcs_clean geant-tcs_upload guides guides_clean guides_upload

all: acme4netvs main geant-tcs guides
upload_all: acme4netvs_upload main_upload index_upload geant-tcs_upload guides_upload
only_upload_all: index_upload acme4netvs_only_upload main_only_upload geant-tcs_only_upload guides_only_upload
clean_all: acme4netvs_clean main_clean geant-tcs_clean guides_clean

main:
	$(MAKE) -C main
main_clean:
	$(MAKE) -C main clean
main_only_upload:
	$(MAKE) -C main only_upload
main_upload:
	$(MAKE) -C main upload

geant-tcs:
	$(MAKE) -C geant-tcs
geant-tcs_clean:
	$(MAKE) -C geant-tcs clean
geant-tcs_only_upload:
	$(MAKE) -C geant-tcs only_upload
geant-tcs_upload:
	$(MAKE) -C geant-tcs upload

acme4netvs:
	$(MAKE) -C acme4netvs
acme4netvs_clean:
	$(MAKE) -C acme4netvs clean
acme4netvs_only_upload:
	$(MAKE) -C acme4netvs only_upload
acme4netvs_upload:
	$(MAKE) -C acme4netvs upload

guides:
	$(MAKE) -C guides
guides_clean:
	$(MAKE) -C guides clean
guides_only_upload:
	$(MAKE) -C guides only_upload
guides_upload:
	$(MAKE) -C guides upload

acme4netvs/docs/en/img/acme4netvs_flow_diagram.webp: acme4netvs/docs/en/img/acme4netvs_flow_diagram.png
	cwebp -progress -z 9 -metadata all $< -o $@

index_upload:
	rsync -vaP --delete-after index.php assets scc-web-0047@docs.ca.kit.edu:docs.ca.kit.edu/htdocs/

