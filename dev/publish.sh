#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
BASE_DIR=$(dirname "$SCRIPT_DIR")

# locate docker or podman binary
CONTAINER_RUNTIME_BIN=$(command -v podman || command -v docker)
if [[ -z "${CONTAINER_RUNTIME_BIN}" ]]; then
	echo "No container binary found. Please install Podman or Docker."
	exit 1
fi

${CONTAINER_RUNTIME_BIN} build -t build_docs_ca:latest "${SCRIPT_DIR}"
${CONTAINER_RUNTIME_BIN} run --rm -v "${BASE_DIR}":/src:Z build_docs_ca:latest make all
make -C "${BASE_DIR}" only_upload_all
