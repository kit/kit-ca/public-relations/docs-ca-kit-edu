#!/usr/bin/env bash

set -e

cd /src

function update() {
	make all
	rsync -vr index.php /var/www/
	rsync -vr main/generated/ /var/www/main/
	rsync -vr acme4netvs/generated/ /var/www/acme4netvs/
	rsync -vr geant-tcs/generated/ /var/www/geant-tcs/
	rsync -vr guides/generated/ /var/www/guides/

	chown -R www-data:www-data /var/www

	echo
	echo "####################################################################################"
	echo "Listening on http://127.0.0.1:3500 if run via JetBrains run configuration 'Preview'."
	echo "####################################################################################"
	echo
}

update
while inotifywait -r -e close_write "/src";
do
	update
done
