# KIT-CA

KIT-CA issues [x509 certificates](https://en.wikipedia.org/wiki/X.509){:target="_blank"}
to all members of KIT.

There are two main areas of application for such certificates:

1. Securing e-mail communication by encrypting and signing e-mails
2. Securing server communication by the operator; at KIT these are mainly web servers

!!! warning "GÉANT TCS: End of contract with Sectigo"
    The contract of GÉANT TCS with Sectigo for operations of the certification authority ends on 10.01.2025.
    KIT-CA will temporarily not be able to issue new certificates.
    All information about this can be found in our [article](/geant-tcs/en/sectigo_contract_ends).

## Certificates for email

A brief explanation of the concepts relevant to you [can be found here](../../geant-tcs/en/concepts).

There is a difference in the application process between certificates for natural persons and the rest (groups,
functional mailboxes, pseudonymized students). The former have to identify themselves regularly, the latter do not.

* [Instructions for requesting user certificates](https://www.ca.kit.edu/p/user)
* [Instructions for requesting group and function certificates](https://www.ca.kit.edu/p/function)
  
[Instructions for setting up certificates in Outlook](/guides/en/configure_outlook/).

Instructions for setting up e-mail clients:

* [Configure Outlook (Windows)](/guides/en/configure_outlook)
* [Configure Apple Mail and Outlook for Mac](/guides/en/install_p12_macos)

## Certificates for servers

We __urgently__ advise all server operators to only obtain certificates from [Let's Encrypt](https://letsencrypt.org/)
automatically. We provide corresponding [documentation](/acme4netvs/en/) and [tooling](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs){:target="_blank"}.

If [ACME](https://de.wikipedia.org/wiki/Automatic_Certificate_Management_Environment){:target="_blank"} clients for Let's Encrypt cannot
be integrated into your infrastructure, you may also obtain a certificate from us (currently from the provider Sectigo via the
GÉANT-TCS project). These are currently only valid for one year. The validity period of server certificates will
probably [soon to be reduced to 90 days](https://www.chromium.org/Home/chromium-security/root-ca-policy/moving-forward-together/){:target="_blank"}.

Instructions for requesting server certificates [are here](https://www.ca.kit.edu/p/server-tcs-en).