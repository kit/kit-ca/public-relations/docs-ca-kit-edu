# KIT-CA

Die KIT-CA stellt allen Mitgliedern des KIT [x509-Zertifikate](https://de.wikipedia.org/wiki/X.509){:target="_blank"} aus.

Es gibt zwei große Anwendungsgebiete für solche Zertifikate:

1. Absichern von E-Mail-Kommunikation mittels Verschlüsseln und Signieren von E-Mails
2. Betreiberseitiges Absichern von Server-Kommunikation; am KIT sind das überwiegend Webserver  

!!! warning "GÉANT TCS: Vertragsende mit Sectigo"
    Der Vertrag von GÉANT TCS mit Sectigo zum Betrieb der Zertifizierungsstelle endet am 10.01.2025.
    Die KIT-CA wird vorübergehend keine neuen Zertifikate ausstellen können.
    Alle Informationen dazu finden Sie in unserem [Artikel](/geant-tcs/de/sectigo_contract_ends).

## Zertifikate für E-Mail

Eine kurze Erklärung der __für Sie__ relevanten Konzepte [finden Sie hier](../../geant-tcs/de/concepts).

Es gibt einen Unterschied im Beantragungsprozess zwischen Zertifikaten für natürliche Personen und dem Rest
(Gruppen, Funktionspostfächer, pseudonymisierte Studierende). Erstere müssen sich regelmäßig identifizieren, Letztere nicht.

* [Anleitung zum Beantragen von Benutzer-Zertifikaten](https://www.ca.kit.edu/p/nutzer)
* [Anleitung zum Beantragen von Gruppen- und Funktionszertifikaten](https://www.ca.kit.edu/p/funktion)

Anleitungen für E-Mail-Clients:

* [Einrichten von Zertifikaten in Outlook](/guides/de/configure_outlook)
* [Apple Mail und Outlook for Mac einrichten](/guides/de/install_p12_macos)

## Zertifikate für Server

Wir raten allen Serverbetreibern __dringend__, nur noch automatisiert Zertifikate von [Let's Encrypt](https://letsencrypt.org/)
zu beziehen. Wir stellen entsprechende [Dokumentation](/acme4netvs/en) und [Tooling](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs){:target="_blank"}
bereit.

Falls [ACME](https://de.wikipedia.org/wiki/Automatic_Certificate_Management_Environment){:target="_blank"}-Clients für Let's Encrypt in
ihre Infrastruktur nicht integrierbar ist, können Sie auch ein Zertifikat von uns (derzeit vom Anbieter Sectigo über das
GÉANT-TCS-Projekt) bekommen, diese sind derzeit nur ein Jahr gültig. Die Laufzeit von Server-Zertifikaten wird sich
wahrscheinlich [demnächst auf 90 Tage reduzieren](https://www.chromium.org/Home/chromium-security/root-ca-policy/moving-forward-together/){:target="_blank"}.

[Hier](https://www.ca.kit.edu/p/server-tcs) finden Sie die entsprechenden Anleitungen.